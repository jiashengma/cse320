#ifndef FUNCTION_H
#define FUNCTION_H

#include "sfmm.h"
#include <string.h>

#define PAGE_SIZE 4096

sf_free_header* getFreeBlock(/*sf_free_header* freelist_head, */size_t size);
size_t getBlockSize(sf_header* header);
size_t getAvailSpace(sf_header* header);
size_t padded_size(size_t size);
void splitFreeBlock(sf_free_header* block, size_t splitted_size/*, sf_free_header* freelist_head*/);
void insert_to_list(sf_free_header* free_block);
void rm_from_list(sf_free_header* allocated_block);
void updateHeader_Footer(sf_header* header, int alloc, size_t block_size, size_t padding);
void updateFooter(sf_header *header);
void coalesce_left(sf_free_header *left, sf_free_header *freed);
void coalesce_right(sf_free_header *freed, sf_free_header *right);
void coalesce_left_right(sf_free_header *left, sf_free_header *freed, sf_free_header *right);
sf_header* getHeader(sf_footer *footer);
sf_footer* getFooter(sf_header *header);
bool isInvalidRange(void *addr);
void updateExternalFragmentation();
//void updateInternalFragmentation();

#endif
