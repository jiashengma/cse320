#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "sfmm.h"
#include "functions.h"

/**
 * All functions you make for the assignment must be implemented in this file.
 * Do not submit your assignment with a main function in this file.
 * If you submit with a main function in this file, you will get a zero.
 */

sf_free_header* freelist_head = NULL;
static int num_of_sfsbrk_call;	/* for tracking the number of sf_sbrk called */

/* keep a valid memory range, inclusive */
static void *range_lower_bound;
static void *range_upper_bound;

/* to keep mem info */
static size_t internal;
static size_t external;
static size_t allocations;
static size_t frees;
static size_t coalesce;


/**
* This is your implementation of malloc. It creates dynamic memory which
* is aligned and padded properly for the underlying system. This memory
* is uninitialized.
* @param size The number of bytes requested to be allocated.
* @return If successful, the pointer to a valid region of memory to use is
* returned, else the value NULL is returned and the ERRNO is set  accordingly.
* If size is set to zero, then the value NULL is returned.
*/
void *sf_malloc(size_t size) {
	void *returnVal;	/* return value */
	sf_free_header *ptr_to_freeBlock_head;
	sf_header *header; /* ptr to available free block */

	int exceededLimit;
	exceededLimit = 0;

	if(size > 0){
		/* 1. check free list, first-fit, 
		   	  if no enough memory, call sf_sbrk(1) 
		*/
		
		/* using a while loop here because malloc should anticipate huge memory request */
		while(1) {
			if((ptr_to_freeBlock_head=getFreeBlock(size)) != NULL && isInvalidRange(ptr_to_freeBlock_head)) {
				/* if there's an available free block for the request, break*/
				break;
			} else {
				/* if there's no availabe free block in the list,  call sf_sbrk */

				/* if sf_sbrk hasnt been called 4 times, 
				   call sf_sbrk(1) to get more memory */
				if(num_of_sfsbrk_call < 4) {
					sf_free_header *new_page_ptr = (sf_free_header *)(sf_sbrk(1) - PAGE_SIZE);
					
					if((void *)new_page_ptr != ((void *)-1)){

						/* new free block, update internal fragmentation */
						// internal = internal + SF_HEADER_SIZE + SF_FOOTER_SIZE;
						
						if(num_of_sfsbrk_call==0) {
							/* keep a lower bound range for later use in freeing */
							range_lower_bound = (void *)new_page_ptr;
							/* max call to sf_sbrk is 4, so set valid uppper bound */
							range_upper_bound = range_lower_bound + 4*PAGE_SIZE;
						}
						/* increment number of sbrk call*/
						num_of_sfsbrk_call++;
						updateHeader_Footer((sf_header *)(&(new_page_ptr->header)), 0, PAGE_SIZE, 0);
						/* add this to free list */
						if(freelist_head == NULL) {
							freelist_head = new_page_ptr;
							freelist_head->prev = NULL;
							freelist_head->next = NULL;
							
						} else {
							/* if left neighbor is unallocated, coalesce with it
								otherwise, insert it to the head of the list */
							sf_footer *footer_L;
							sf_free_header *block_L;
							footer_L = (sf_footer *)(((void *)(new_page_ptr) - SF_FOOTER_SIZE));
							if(footer_L->alloc == 0) {
								block_L = (sf_free_header *)(((void *)footer_L + SF_FOOTER_SIZE - (footer_L->block_size << 4)));
								coalesce_left(block_L, new_page_ptr);
							} else {
								insert_to_list(new_page_ptr);
							}
						}
					} else {
						/* error in sbrk*/
						errno = ENOMEM;
						returnVal = NULL;
						break;
					}
					
				} else {
					/* no more sf_sbrk is allowed. Set ERRNO? */
					exceededLimit = 1; /* limit is exceeded, flag is up */
					errno = ENOMEM;
					returnVal = NULL;
					break;
				}
			}
		}

		/* if there's no failure in requesting memory, then proceed*/
		if(exceededLimit == 0) {
			/* retreive header from list */
			header = &(ptr_to_freeBlock_head->header);
	
			/* if an available block's header is returned,
				   proceed to further steps */
		
			/* check splinter */
			if((getBlockSize(header)-padded_size(size)-SF_HEADER_SIZE-SF_FOOTER_SIZE) < 32) {
				/* if the leftover space is less than 32 bytes,
				   then it causes splinter,
	
				   allocate it all, 
				   1. remove the block from the free list
				   2. update header and footer 
				   3. move ptr to payload */
				rm_from_list(ptr_to_freeBlock_head);
				updateHeader_Footer(header, 1, getBlockSize(header),padded_size(size)-size);

				/* update internal fragmentation */
				internal = internal + (padded_size(size)-size) + 2*8;

			} else {
				/* if no splinter is caused, split the block, update the footer, add header */
				splitFreeBlock(ptr_to_freeBlock_head, SF_HEADER_SIZE+padded_size(size)+SF_FOOTER_SIZE);

				/* update internal fragmentation from the allocated block
					the splitted(free) block is taken care in splitFreeBlock function */
				internal = internal + (padded_size(size)-size) + 2*8;

				/* update the allocated block's header and footer */
				updateHeader_Footer(header, 1, SF_HEADER_SIZE+padded_size(size)+SF_FOOTER_SIZE, padded_size(size)-size);
			}
	
			/* move header ptr to payload, return memory address */
			returnVal = (void *)(header) + SF_HEADER_SIZE;
		}
	} else if(size == 0) {
		returnVal = NULL;
		errno = ENOMEM;
	} else {
		/* what to return when size is negative?
			-> size_t is alway positive, do nothing
		 */
	}

	/* if request fails, return NULL */
	if(returnVal!=NULL && exceededLimit==0) {
		/* update allocation count if available memory space is found */
		allocations++;
	}
	updateExternalFragmentation();
	
	/* return address of payload */
	return returnVal;
}

/**
* Marks a dynamically allocated region as no longer in use.
* Adds the newly freed block to the free list.
* @param ptr Address of memory returned by the function sf_malloc.
*/
void sf_free(void *ptr){

	/* if ptr is not a NULL ptr, proceed */
	if(ptr != NULL && isInvalidRange(ptr)){
		/* go back to header,
		   1. check neighboring blocks to determine coalescing case */
		size_t alloc_bit_L, alloc_bit_R, blockSize;
		sf_header *block_to_free;
		sf_footer *block_to_free_ft;
		sf_footer *footer_L;
		sf_header *header_R;
		sf_free_header *block_R, *block_L, *block_freeing;
	
		block_to_free = (sf_header*)(ptr - SF_HEADER_SIZE);

		/* check if header is in valid range, 
			if yes, next, check if header and footer, block sizes match
			if no, return
		*/
		if(isInvalidRange(block_to_free)){
			blockSize = getBlockSize(block_to_free);
			block_to_free_ft = (sf_footer *)(((void *)block_to_free) + blockSize - SF_FOOTER_SIZE);
			/* check if header and footer, block sizes match
				if yes, proceed to free, but may have undefined behavior
				if no, return 
			 */
			if((block_to_free->alloc == block_to_free_ft->alloc) 
			&& (block_to_free->block_size == block_to_free_ft->block_size)
			&& (block_to_free->alloc == 1)
			&& (block_to_free_ft->alloc == 1)) {
						
				/* 			Go to footer of left, get alloc bit
				#################################################################
				##### BE SURE TO CHECK IF FOOTER IS WITHIN THE VALID RANGE! #####
				#################################################################
							if it isnt, set alloc bit to -1 to indicate error */
				footer_L = (sf_footer *)((void *)(block_to_free) - SF_FOOTER_SIZE);
				if(isInvalidRange(footer_L)) {
					alloc_bit_L = footer_L->alloc;
				} else {
					alloc_bit_L = -1;
				}
				
				/* 			Go to header of right, get alloc bit. 
				#################################################################
				##### BE SURE TO CHECK IF HEADER IS WITHIN THE VALID RANGE! #####
				#################################################################
							if it isnt, set alloc bit to -1 to indicate error */
				header_R = (sf_header *)((void *)(block_to_free) + blockSize);
				if(isInvalidRange(header_R)) {
					alloc_bit_R = header_R->alloc;
				} else {
					alloc_bit_R = -1;
				}
				
				/* get left and right and freeing blocks(for coalescing) */	
				block_freeing = (sf_free_header*)block_to_free;
				if(alloc_bit_R != -1) {
					block_R = (sf_free_header *)header_R;
				}
				if(alloc_bit_L != -1) {
					block_L = (sf_free_header *)(((void *)footer_L) + SF_FOOTER_SIZE - (footer_L->block_size << 4));
				}

				/* update internal frag: padding is no longer internal frag*/
				internal = internal - block_to_free->padding_size - 2*8;
			
				/* decide coalescing case */
				if(alloc_bit_L==0 && alloc_bit_R==0) {
					/* coalesce with left and right neighbors */
					coalesce_left_right(block_L, block_freeing, block_R);
				} else if(alloc_bit_L==0) {
					/* coalesce with left neighbor */
					coalesce_left(block_L, block_freeing);
				} else if(alloc_bit_R==0) {
					/* coalesce with right neighbor */
					coalesce_right(block_freeing, block_R);
				} else {
					//if((alloc_bit_L==1 && alloc_bit_R==1) || (alloc_bit_L==-1 && alloc_bit_R==-1)) {
					/* if both neighbors are allocated or dont exit, just free and update header and footer */
					updateHeader_Footer(block_to_free, 0, blockSize, 0);
					/* insert the freed block to the head of the list */
					insert_to_list((sf_free_header*)block_to_free);
				}
			} else {
				/* invalid payload addr, return */
				return;
			}
		} else {
			/* invalid range, return */
			return;
		}
	} else {
		/* mem address to be freed is
		   NULL ptr/ in invalide range, do nothing */
		return;
	}

	/* update info after freeing and possible coalescing */
	frees++;

	updateExternalFragmentation();
}

/**
 * Resizes the memory pointed to by ptr to be size bytes.
 * @param ptr Address of the memory region to resize.
 * @param size The minimum size to resize the memory to.
 * @return If successful, the pointer to a valid region of memory to use is
 * returned, else the value NULL is returned and the ERRNO is set accordingly.
 *
 * A realloc call with a size of zero should return NULL and set the ERRNO
 * accordingly.
 */
void *sf_realloc(void *ptr, size_t size){
	/*	1. get ptr's payload size and compare it to new size
			if the result indicates a shrinking of memoery
				then just update header and footer
			else if the result indicates an expansion
				then if the right neighbor has enough free space, merge the two blocks
				update header footer

		. keep the content in the payload 
		. find available block by using sf_malloc
			if there's available block (sf_malloc returning a valid addr)
				i.  copy content to payload
				ii. free old block
			else
				return NULL, set ERRNO
	*/

	void *returnVal;
	sf_header *old_block;
	size_t blockSize;
	/* enforce check of validity of ptr */ 
	if(ptr != NULL && isInvalidRange(ptr)) {
		old_block = (sf_header *)(ptr - SF_HEADER_SIZE);
		/* check payload header's validity */
		if(isInvalidRange(old_block)){
			sf_footer *old_footer;
			old_footer = (sf_footer *)((void *)old_block + getBlockSize(old_block) - SF_FOOTER_SIZE);

			/* Check header and corresponding footer's */
			if((old_block->block_size==old_footer->block_size) && (old_block->alloc==old_footer->alloc)) {
				if(size > 0) {
					/* check shrink or expand */ 
					blockSize = getBlockSize(old_block);
					if(getAvailSpace(old_block) > padded_size(size)) {
						/* shrink => make sure no splinter
							if  splinter
								do nothing 
							else  
								just update header and footer
						*/
						if((blockSize-SF_HEADER_SIZE-padded_size(size)-SF_FOOTER_SIZE) < 32) {
							/* do nothing, splinter, return same addr! */
							/* update internal frag */
							internal = internal - old_block->padding_size;
							internal = internal + (padded_size(size)-size);
							returnVal = ptr;
						} else {
							/* update header and footer
							   add the leftover part to the free list */
							size_t new_blockSize;
							sf_free_header *leftover_freeBlock;
	
							new_blockSize = SF_HEADER_SIZE + padded_size(size) + SF_FOOTER_SIZE; 
							updateHeader_Footer(old_block, 1, new_blockSize, padded_size(size)-size);
	
							/* create free block from leftover space, update header and footer */
							leftover_freeBlock = (sf_free_header *)((void *)old_block + new_blockSize);
							updateHeader_Footer(&(leftover_freeBlock->header), 0, blockSize-new_blockSize, 0);
							insert_to_list(leftover_freeBlock);

							/* update internal fragmentation, splitting*/
							internal = internal - old_block->padding_size;
							internal = internal + (padded_size(size)-size);// + SF_HEADER_SIZE + SF_FOOTER_SIZE;

							/* splitting old block, allocations++, free++ */
							allocations++;
							frees++;

							returnVal = ((void *)old_block) + 8;
						}
	
					} else if(getAvailSpace(old_block) < padded_size(size)) {
						/* reallocation for larger space */
						/* get header of block at the right */
						sf_header *header_R;
						header_R = (sf_header *)((void *)old_block + blockSize);
	
						if((header_R->alloc == 0) && ((getAvailSpace(old_block) + SF_FOOTER_SIZE + 
							SF_HEADER_SIZE + getAvailSpace(header_R)) > padded_size(size))) {
							/*  if there's large enough space when combined with the FREE 
							    right neighbor, then
									coalesce (remove free right neighbor from freelist) 
									allocate enough mem, split leftover if necessary
									update header and footer 
									and return the same addr
							*/
							/* create a new block, with a total space of old and right neighbor */
							sf_header *new_block;
							size_t leftover_blockSize;
							/* remove right from free list */
							rm_from_list((sf_free_header *)header_R);
	
							/* temporary reduce internal fragmentation count, recalculate in the splinter check */
							internal = internal - old_block->padding_size;

							new_block = old_block;	/* same old address, but updated header */
							updateHeader_Footer(new_block, 1, blockSize+getBlockSize(header_R), padded_size(size)-size);
							/* check if the combined space minus the padded requested size
							   would cause a splinter */
							leftover_blockSize = getBlockSize(new_block)-SF_HEADER_SIZE-padded_size(size)-SF_FOOTER_SIZE; 
							if(leftover_blockSize < 32) {
								/* splinter! return the entire new block */
								internal = internal + (padded_size(size)-size);
								returnVal = ((void *)new_block) + 8;

								allocations++;
							} else {
								/* split the new_block */
								sf_free_header *leftover_freeBlock;
								/* create free block from leftover space, update header and footer */
								leftover_freeBlock = (sf_free_header *)((void *)new_block+padded_size(size)+SF_FOOTER_SIZE);
								updateHeader_Footer(&(leftover_freeBlock->header), 0, leftover_blockSize, 0);
	
								insert_to_list(leftover_freeBlock);
	
								/* update (again) new_block header and footer */
								updateHeader_Footer(new_block, 1, SF_HEADER_SIZE+padded_size(size)+SF_FOOTER_SIZE, padded_size(size)-size);
								
								/* update internal fragmentation, splitting*/
								internal = internal /*+ 2*8 */ + new_block->padding_size;
								
								/* block is splitted allocation++ ... */
								allocations++;

								returnVal = ((void *)new_block) + 8;
							}

						} else {
							/*  1. save a copy of the data in the old mem space
							    2. use malloc to find new block
							    3. move data to new space
							    4. free this block */
							void *new_ptr;
							new_ptr = sf_malloc(size);

							if(new_ptr!=NULL && isInvalidRange(new_ptr)){
								/* move date to new mem addr*/
								memmove(new_ptr, ptr, getAvailSpace(old_block));
								sf_free(ptr);
								returnVal = new_ptr;
							} else {
								returnVal = NULL;
								errno = EINVAL;
							}
						}
					} else {
						/* realloc size is the same as old size 
							do nothing */
						returnVal = ptr;
					}
		
				} else {
					/* invalid request, return NULL */
					returnVal = NULL;
					/* set ERRNO */
					errno = EINVAL;
		
				}
			} else { /* invalid header and footer */
				returnVal = NULL;
				errno = EINVAL;
			}
		} else { /* invalid range */
			returnVal = NULL;
			errno = EINVAL;
		}
	} else { /* invalid ptr*/
		returnVal = NULL;
		errno = EINVAL;
	}
	
	/* update info */
	/*if(returnVal != NULL && getAvailSpace(old_block) != padded_size(size)) {
		allocations++;
	}*/

	updateExternalFragmentation();
	return returnVal;
}

/**
 *  copies memory info to info struct
 *  @param meminfo A pointer to the memory info struct passed
 *  to the function, upon return it will containt the calculated
 *  for current fragmentation
 *  @return If successful return 0, if failure return -1
 */
int sf_info(info* meminfo) {
	/*
	updateExternalFragmentation();
	updateInternalFragmentation();
	*/
	if(meminfo != NULL){
		meminfo->internal = internal;
		meminfo->external = external;
		meminfo->allocations = allocations;
		meminfo->frees = frees;
		meminfo->coalesce = coalesce;
		return 0;
	} else {
		return -1;
	}
}


/****************************************************************/
/*************          helper_functions         ****************/
/****************************************************************/

/**
 * Checks the free list to find the first fit block
 * If there's an available block found, 
 * return its address in the free list
 * Otherwise return NULL
 * ###(NOTE: this function does not check splinter, the sf_malloc should)
 * 
 * @param freelist_head: head of free list
 * @param size: requested size
 */
sf_free_header* getFreeBlock(/*sf_free_header *freelist_head, */size_t size) {
	sf_free_header *cursor;	/* cursor for traversing the free list */
	sf_free_header *returnVal;
	
	returnVal = NULL;
	cursor = freelist_head;

	if(cursor == NULL) {
		returnVal = NULL;
	} else {
		while(isInvalidRange(cursor)) {
			/* pad before comparing, 
			   because a free block must be larger enough to hold padding */
			if((getAvailSpace(&(cursor->header)) >= padded_size(size)) && padded_size(size) >= 16) {
				returnVal = cursor;
				break;
			}
			/* check next free block */
			cursor = cursor->next;
		}
	}

	return returnVal;
}

size_t getBlockSize(sf_header *header) {
	return (header->block_size) << 4;
}

/**
 * Returns the usable space in the block, in bytes.
 *
 */
size_t getAvailSpace(sf_header *header) {
	size_t availSpace;
	availSpace = getBlockSize(header) - SF_HEADER_SIZE - SF_FOOTER_SIZE;

	return availSpace;
}

/**
 * Returns the size that is padded(i.e. divisible by 16)
 * to ensure a valid memory block is returned.
 */
size_t padded_size(size_t size) {
	if(size%16 == 0)
		return size;
	return ((size/16)+1)*16;
}

/**
 * 1. Splits the block into allocated and unallocated.
 * 2. Removes the whole block then adds the unallocated 
 *    to the head of the free list.
 * 3. Updates references of the new free block in the list
 * 
 * After splitting the block, the new free block's address 
 *        should replace the allocated one's
 * @param block: ptr to header of block to be splitted
 * @param splitted_size: size of block splitted from this block
 * @param freelist_head: head of free list
 */
void splitFreeBlock(sf_free_header *block, size_t splitted_size/*, sf_free_header* freelist_head*/) {
	size_t new_block_size;
	sf_free_header *new_free_block_header;
	sf_header *new_header;

	/* 1. remove allocated block from free list */
	rm_from_list(block);

	
	new_block_size = getBlockSize(&(block->header)) - splitted_size;
	/* 2. split */
	new_free_block_header = (sf_free_header*)((void *)block + splitted_size);
	/* update internal fragmenation: one block splits into two->header+footer */
	//internal = internal + SF_HEADER_SIZE + SF_FOOTER_SIZE;
	
	/* update header*/
	new_header = &(new_free_block_header->header);
	updateHeader_Footer(new_header, 0, new_block_size, 0);

	/* 3. insert new block to head of free list */
	insert_to_list(new_free_block_header);
	
}

/**
 * Inserts block to head of list
 */
void insert_to_list(sf_free_header* free_block) {
	if(freelist_head == NULL) {
		freelist_head = free_block;
		freelist_head->prev = NULL;
		freelist_head->next = NULL;
	} else {
		free_block->prev = NULL;			/* new head's prev is NULL */
		free_block->next = freelist_head;	/* new head's next is current head */
		freelist_head->prev = free_block;	/* current head's prev is new head */
		freelist_head = free_block;			/* update current head to new head */
	}
	
}

/**
 * Removes allocated block from the free list
 */
void rm_from_list(sf_free_header* allocated_block) {
	if(allocated_block->next == NULL && allocated_block->prev == NULL) {
		freelist_head = NULL;
	} else {
		if(allocated_block->prev == NULL) {	
			/* which means head is being freed */
			
			/* set head's next's prev to be NULL*/
			allocated_block->next->prev = NULL;
			/* set next to be new head */
			freelist_head = allocated_block->next;
		} else if(allocated_block->next == NULL) {
			/* tail is being freed, just set tail's next to be NULL */
			allocated_block->prev->next = NULL;
		} else {
			/* block in the middle of the list is being free, swap references: 
				prev's next => next
				next's prev => prev */
			allocated_block->prev->next = allocated_block->next;
			allocated_block->next->prev = allocated_block->prev;
		}
	}

}

/**
 * Updates header
 * 
 * @param alloc: allocated bit flag: 0 is free, 1 is allocated (none zeroes are treated as allocated)
 * @param block_size: block size for the malloc request (in bytes)
 * @param padding: padding for the malloc request in bytes
 */
void updateHeader_Footer(sf_header* header, int alloc, size_t block_size, size_t padding) {
	/* 1. set allocated bit to */
	if(alloc == 0) {
		header->alloc = 0;
	} else {
		header->alloc = 1;
	}
	block_size >>= 4;
	header->block_size = block_size;
	header->padding_size = padding;

	/* update footer */
	updateFooter(header);
}

/** 
 * updates footer ###(NOTE: the header must be updated before updating footer)
 * @param: header: footer's corresponding header
 */
void updateFooter(sf_header *header) {
	/* go to the footer from the header */
	sf_footer *footer;
	footer = (sf_footer*)((void *)header + getBlockSize(header) - SF_FOOTER_SIZE);
	footer->alloc = header->alloc;
	footer->block_size = header->block_size;

}

void coalesce_left(sf_free_header *left, sf_free_header *freed) {
	/* remove left from free list */
	rm_from_list(left);

	/* update block size in header/footer: coalesce */
	/* header of left and footer of freed should be updated*/
	size_t new_block_size = getBlockSize(&(left->header)) + getBlockSize(&(freed->header));
	updateHeader_Footer(&(left->header), 0, new_block_size, 0);

	/* insert new block*/
	insert_to_list(left);
	/* update coalesce count */
	coalesce++;
	/* update internal: two free blocks coalesce into one-> 2*8 less*/
	//internal = internal-2*8;
}

void coalesce_right(sf_free_header *freed, sf_free_header *right) {
	/* remove right from free list */
	rm_from_list(right);

	/* update block size in header/footer: coalesce */
	/* header of freed and footer of right should be updated*/
	size_t new_block_size = getBlockSize(&(freed->header)) + getBlockSize(&(right->header));
	updateHeader_Footer(&(freed->header), 0, new_block_size, 0);

	/* insert new block*/
	insert_to_list(freed);
	/* update coalesce count */
	coalesce++;
	/* update internal: two free blocks coalesce into one-> 2*8 less*/
	//internal = internal-2*8;
	
}

void coalesce_left_right(sf_free_header *left, sf_free_header *freed, sf_free_header *right) {
	/* remove left and right from free list, insert new block*/
	rm_from_list(left);
	rm_from_list(right);

	/* update block size in header/footer: coalesce */
	/* header of left and footer of right should be updated*/
	size_t new_block_size = getBlockSize(&(left->header)) 
						  + getBlockSize(&(freed->header)) 
						  + getBlockSize(&(right->header));
	updateHeader_Footer(&(left->header), 0, new_block_size, 0);

	/* insert new block*/
	insert_to_list(left);
	/* update coalesce count */
	coalesce++;
	/* update internal: 3 free blocks coalesce into one-> 2*8 less*/
	//internal = internal-4*8;
}

/**
 * Returns header of the corresponding footer.
 */
sf_header* getHeader(sf_footer *footer) {
	size_t blockSize;
	blockSize = (footer->block_size) << 4;
	return (sf_header *)(((void *)footer) + SF_FOOTER_SIZE - blockSize); 
}

/**
 * Returns footer of the corresponding header.
 */
sf_footer* getFooter(sf_header *header) {
	size_t blockSize;
	blockSize = getBlockSize(header);
	return (sf_footer *)(((void *)(header)) + blockSize - SF_FOOTER_SIZE);
}

bool isInvalidRange(void *addr) {
	if(addr >= range_lower_bound && addr <= range_upper_bound)
		return true;
	return false;
}

void updateExternalFragmentation() {
	sf_free_header *cursor;
	cursor = freelist_head;
	/* reset */
	external = 0;
	/* update */
	while(cursor!=NULL) {
		external += (getBlockSize(&(cursor->header))/* - SF_HEADER_SIZE - SF_FOOTER_SIZE*/);
		cursor = cursor->next;
	}
}

/**
 * Updates the program's current internal fragmentation 
 * by going through every block
 */
// void updateInternalFragmentation() {
// 	sf_header *cursor = (sf_header *)range_lower_bound;
// 	//void *current_sbrk = sf_sbrk(0);
// 	/* reset internal fragmentation */
// 	internal = 0;
// 	while((((range_lower_bound+num_of_sfsbrk_call*PAGE_SIZE) - (void*)cursor) > 0)
// 		&&((range_lower_bound+num_of_sfsbrk_call*PAGE_SIZE) - (void*)cursor) != 0) {
// 		 header is infrag 
// 		internal += SF_HEADER_SIZE;
// 		/* if the current block is allocated, add padding as internal frag*/
// 		if(cursor->alloc == 1) {
// 			internal += (cursor->padding_size);
// 		}
// 		/* footer is internal frag*/
// 		internal += SF_FOOTER_SIZE;

// 		/* move cursor */
// 		cursor = (sf_header*)(((void*)(cursor)) + (cursor->block_size << 4));
// 	}
// }
