#include <criterion/criterion.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sfmm.h"

/**
 *  HERE ARE OUR TEST CASES NOT ALL SHOULD BE GIVEN STUDENTS
 *  REMINDER MAX ALLOCATIONS MAY NOT EXCEED 4 * 4096 or 16384 or 128KB
 */

Test(sf_memsuite, Malloc_an_Integer, .init = sf_mem_init, .fini = sf_mem_fini) {
    int *x = sf_malloc(sizeof(int));
    *x = 4;
    cr_assert(*x == 4, "Failed to properly sf_malloc space for an integer!");
}

Test(sf_memsuite, Free_block_check_header_footer_values, .init = sf_mem_init, .fini = sf_mem_fini) {
    void *pointer = sf_malloc(sizeof(short));
    sf_free(pointer);
    pointer = pointer - 8;
    sf_header *sfHeader = (sf_header *) pointer;
    cr_assert(sfHeader->alloc == 0, "Alloc bit in header is not 0!\n");
    sf_footer *sfFooter = (sf_footer *) (pointer - 8 + (sfHeader->block_size << 4));
    cr_assert(sfFooter->alloc == 0, "Alloc bit in the footer is not 0!\n");
}

Test(sf_memsuite, PaddingSize_Check_char, .init = sf_mem_init, .fini = sf_mem_fini) {
    void *pointer = sf_malloc(sizeof(char));
    pointer = pointer - 8;
    sf_header *sfHeader = (sf_header *) pointer;
    cr_assert(sfHeader->padding_size == 15, "Header padding size is incorrect for malloc of a single char!\n");
}

Test(sf_memsuite, Check_next_prev_pointers_of_free_block_at_head_of_list, .init = sf_mem_init, .fini = sf_mem_fini) {
    int *x = sf_malloc(4);
    memset(x, 0, 4);
    cr_assert(freelist_head->next == NULL);
    cr_assert(freelist_head->prev == NULL);
}

Test(sf_memsuite, Coalesce_no_coalescing, .init = sf_mem_init, .fini = sf_mem_fini) {
    void *x = sf_malloc(4);
    void *y = sf_malloc(4);
    memset(y, 0xFF, 4);
    sf_free(x);
    cr_assert(freelist_head == x-8);
    sf_free_header *headofx = (sf_free_header*) (x-8);
    sf_footer *footofx = (sf_footer*) ((x - 8 + (headofx->header.block_size << 4)) - 8);

    sf_blockprint((sf_free_header*)((void*)x-8));
    // All of the below should be true if there was no coalescing
    cr_assert(headofx->header.alloc == 0);
    cr_assert(headofx->header.block_size << 4 == 32);
    cr_assert(headofx->header.padding_size == 0);

    cr_assert(footofx->alloc == 0);
    cr_assert(footofx->block_size << 4 == 32);
}

/*
//############################################
// STUDENT UNIT TESTS SHOULD BE WRITTEN BELOW
// DO NOT DELETE THESE COMMENTS
//############################################
*/

/* Test case 6: check block size of coalescing with left*/
Test(sf_memsuite, Coalesce_coalesce_with_left, .init = sf_mem_init, .fini = sf_mem_fini) {
	// printf("%s\n", "*** Test 6 ***");

	/* first free the left, then the middle */
	void *left = sf_malloc(4);
	void *middle = sf_malloc(8);
	void *right = sf_malloc(sizeof(int));

	/* check if the alloc bit of the left is 1*/
	cr_assert(((sf_header *)(left-8))->alloc == 1);
	/* check if block size of left is 32 */
	cr_assert(((sf_header *)(left-8))->block_size << 4 == 32);
	
	sf_free(left);
	/* check if the alloc bit of the left is 0*/
	cr_assert(((sf_header *)(left-8))->alloc == 0);
	sf_free(middle);
	/* check if coalesced block, left's header has block size 64*/
	cr_assert(((sf_header *)(left-8))->block_size << 4 == 64);
	/*check footer of middle to see if it is 64 after coalescing*/
	cr_assert(((sf_footer *)(right-16))->block_size << 4 == 64);
}

/* Test case 7: check block size of coalescing with right*/
Test(sf_memsuite, Coalesce_coalesce_with_right, .init = sf_mem_init, .fini = sf_mem_fini) {
	// printf("%s\n", "*** Test 7 ***");

	/* first free the left, then the middle */
	void *left = sf_malloc(16);
	void *middle = sf_malloc(8);
	void *right = sf_malloc(33);

	/* check if the padding of left is 0*/
	cr_assert(((sf_header *)(left-8))->padding_size == 0);
	/* check if the alloc bit of the right is 1*/
	cr_assert(((sf_header *)(right-8))->alloc == 1);
	/* check if block size of left is 64 */
	cr_assert(((sf_header *)(right-8))->block_size << 4 == 64);
	/* check if padd of right is 48 - 33 = 15*/
	cr_assert(((sf_header *)(right-8))->padding_size == 15);

	sf_free(right);
	/* check if the alloc bit of the right is 0*/
	cr_assert(((sf_header *)(right-8))->alloc == 0);
	sf_free(middle);
	/* check if coalesced block, middle's header has 
		(block size of a page - block size of left) 
	*/
	cr_assert(((sf_header *)(middle-8))->block_size << 4 == (4096 - (((sf_header *)(left-8))->block_size << 4)));
	/*check footer of right(the right most footer) to see if it is 
		***(block size of a page - block size of left)**
		after coalescing*/
	cr_assert(((sf_footer *)((left-8) + 4096 - 8))->block_size << 4 == (4096 - (((sf_header *)(left-8))->block_size << 4)));
}

/* Test case 8: check coalescing both left and right*/
Test(sf_memsuite, Coalesce_coalesce_left_right, .init = sf_mem_init, .fini = sf_mem_fini) {
	//printf("%s\n", "*** Test 8 ***");

	void *left = sf_malloc(10);
	void *middle = sf_malloc(15);
	void *right = sf_malloc(64);

	/* assert block size of block_left and block right */
	cr_assert((((sf_header *)(left - 8))->block_size << 4) == 32);
	cr_assert((((sf_header *)(right - 8))->block_size << 4) == (64+16));

	/* free both left and right */
	cr_assert((((sf_header *)(left - 8))->alloc) == 1);
	cr_assert((((sf_header *)(right - 8))->alloc) == 1);
	sf_free(left);
	sf_free(right);
	cr_assert((((sf_header *)(left - 8))->alloc) == 0);
	cr_assert((((sf_header *)(right - 8))->alloc) == 0);

	/* free middle, assert total block size 4096*/
	sf_free(middle);
	cr_assert((((sf_header *)(left - 8))->block_size << 4) == 4096);
	cr_assert((((sf_footer *)(left-8+4096-8))->block_size << 4) == 4096);

}

/* Test case 9: validate sf_realloc return value */
Test(sf_memsuite, sf_realloc_Return_value, .init = sf_mem_init, .fini = sf_mem_fini) {
	// printf("%s\n", "*** Test 9 ***");

	void *ptr = sf_malloc(16);
	void *ptr2 = ptr;
	
	/*******************************************************************
		The memoery structure right now is(from the start of the heap): 
	>>	[alloc'd, 32 bytes] 
	********************************************************************/
	/* assert ptr2 is the same as ptr before sf_realloc*/
	cr_assert(ptr2 == ptr);
	/* assert ptr2 is the same as ptr after sf_realloc (shrinking)*/
	ptr = sf_realloc(ptr, 4);
	cr_assert(ptr2 == ptr);
	
	/*******************************************************************
		The memoery structure right now is(from the start of the heap): 
	>>	[alloc'd, 32 bytes] 
	********************************************************************/
	/* assert expansion of memory does not move ptr if there's available space to the right */
	ptr = sf_realloc(ptr, 1000);
	/*******************************************************************
		The memoery structure right now is(from the start of the heap): 
	>>	[alloc'd, 1000 bytes] 
	********************************************************************/
	cr_assert(ptr2 == ptr);

	void *block_to_realloc = sf_malloc(sizeof(int));
	void *block_right = sf_malloc(sizeof(int));
	void *ptr_btr = block_to_realloc; /* keep a copy of ptr to block_to_realloc */
	/*******************************************************************
		The memoery structure right now is(from the start of the heap): 
	>>	[alloc'd, 1000 bytes]->[alloc'd, 32 bytes]->[alloc'd, 32 bytes] 
	********************************************************************/

	/* realloc the middle for more memory, assert it need to malloc for new block
		and the block is right after block_right
	*/
	block_to_realloc = sf_realloc(block_to_realloc, 48);
	/* get the address of the header right after block_right, assert if it is 
		the same address as block_to_realloc's new address */
	void *block_after_right = (block_right-8) + ((((sf_header*)(block_right-8))->block_size) << 4);
	cr_assert(block_to_realloc-8 == block_after_right);

	/*******************************************************************
		The memoery structure right now is(from the start of the heap): 
	>>	[alloc'd, 1000 bytes]->[UNalloc'd, 32 bytes]->[alloc'd, 32 bytes] 
		->[alloc'd, 64 bytes] 
	********************************************************************/
	/* now sf_malloc(sizeof(int)) to fill in the second block, assert it*/ 
	void *new_second_block = sf_malloc(sizeof(int));
	cr_assert(new_second_block == ptr_btr);
}

/* Test case 10: validate mem info */
Test(sf_memsuite, mem_info, .init = sf_mem_init, .fini = sf_mem_fini) {
	//printf("%s\n", "*** Test 10 ***");

	info meminfo;
	
	int rtv = sf_info(&meminfo);
	/* assert return value of sf_info is 0 */
	cr_assert(rtv == 0);

	/* assert original internal frag */
	cr_assert(meminfo.internal == 0);


	/* assert no coalesce */
 	cr_assert(meminfo.coalesce == 0);

 	/* first sbrk, update meminfo*/
 	void *block1 = sf_malloc(8);
 	sf_info(&meminfo);

 	/* assert internal frag after allocating 8 bytes */
 	cr_assert(meminfo.internal == 8+((sf_header*)(block1-8))->padding_size+8);

	/* assert external fragmentation: amount of free space */
	cr_assert(meminfo.external == (4096-((((sf_header*)(block1-8))->block_size)<<4)));

	/* request for 5000 bytes, 2nd sbrk would have a coalesce */
	void *block2 = sf_malloc(5000);
	sf_info(&meminfo); /* get updated mem info */
	cr_assert(meminfo.coalesce == 1);

	/* assert internal frag after allocating additional 5000 bytes */
	cr_assert(meminfo.internal == 8+((sf_header*)(block1-8))->padding_size+8 
 		+ 8+ ((sf_header*)(block2-8))->padding_size +8);

	sf_free(block1);
	/* assert coalesce is still 1*/
	sf_info(&meminfo); /* get updated mem info */
	cr_assert(meminfo.coalesce == 1);

	/* assert internal frag after freeing block 1 */
 	cr_assert(meminfo.internal = (8+ ((sf_header*)(block2-8))->padding_size +8));

	sf_free(block2);
	/* assert coalesce is 2*/
	sf_info(&meminfo); /* get updated mem info */
	cr_assert(meminfo.coalesce == 2);

	/* assert number of frees */
	cr_assert(meminfo.frees==2);
	/* assert number of allocation */
	cr_assert(meminfo.allocations==2);

	/* assert external fragmentation: amount of free space, 
	                                  2 pages of free space */
	cr_assert(meminfo.external == (4096+4096));

	/* allocate 3 blocks and free the middle to create a gap, 
		assert external fragmentation */
	void *block_L = sf_malloc(45);
	void *block_M = sf_malloc(31);
	void *block_R = sf_malloc(8);

	sf_free(block_M);
	sf_info(&meminfo);	/* update meminfo*/

	cr_assert(meminfo.external == (4096+4096		/* total external frag before malloc */
		-((((sf_header*)(block_L-8))->block_size)<<4)	/* external frag after malloc'd for block_L */
		-((((sf_header*)(block_M-8))->block_size)<<4)	/* external frag after malloc'd for block_M */
		-((((sf_header*)(block_R-8))->block_size)<<4)	/* external frag after malloc'd for block_R */
		+(((((sf_header*)(block_M-8))->block_size<<4))))); /* external frag after freeing block_L */

	/* assert internal fragmentation */
	/*  [alloc'd (block_L)]			(16B+((sf_header*)(block_L-8))->padding_size internal frag)
	  ->[freed (block_M)]			(16B internal frag)
	  ->[alloc'd (block_R)]         (16B+((sf_header*)(block_R-8))->padding_size internal frag)
	  ->[UNalloc'd freelist_head]   (16B internal frag)*/
	cr_assert(meminfo.internal == 
		     ((2*8+((sf_header*)(block_L-8))->padding_size)
		     //+(2*8)
		     +(2*8+((sf_header*)(block_R-8))->padding_size)
		     //+(2*8)
		     ));
}

/* Test case 11: validate freelist */
Test(sf_memsuite, validate_freelist_blocks, .init = sf_mem_init, .fini = sf_mem_fini) {
	/* assert freelist_head is NULL before sf_malloc */
	cr_assert(freelist_head == NULL);
	void *block1 = sf_malloc(4096 - 2*8);
	/* assert free list is still NULL after allocating a page of memory */
	cr_assert(freelist_head == NULL);
	sf_free(block1);
	/* assert freelist is now has the freed block 1*/
	cr_assert(freelist_head == block1-8);

	/* to compare with allocating a page and asserting free list is NULL,
	   now allocate a byte, and assert freelist isnt NULL */
	void *block2 = sf_malloc(1);
	cr_assert(freelist_head != NULL);
	cr_assert(freelist_head == block2-8+(((sf_header*)(block2-8))->block_size << 4));
	
	/* allocate large memory, 2nd sf_sbrk,
		freelist now should start at block size of block 2 + block 3 
		away from the head of block 1*/
	void *block3 = sf_malloc(4096);
	cr_assert(freelist_head == ((block1-8)
		+((((sf_header*)(block2-8))->block_size << 4)
		+(((sf_header*)(block3-8))->block_size << 4))));
}


/* Test case 12: malloc invalid values  */
Test(sf_memsuite, malloc_invalid_values, .init = sf_mem_init, .fini = sf_mem_fini) {
	int *val1 = sf_malloc(4*4096);
	cr_assert(val1 == NULL);

	double *val2 = sf_malloc(0);
	cr_assert(val2 == NULL);

	long *val3 = sf_malloc(-8);
	cr_assert(val3 == NULL);

	int *val4 = sf_malloc(100*4096);
	cr_assert(val4 == NULL);

	int *val5 = sf_malloc(1234*4096);
	cr_assert(val5 == NULL);

	int *val6 = sf_malloc(-43142);
	cr_assert(val6 == NULL);
}

/* Test case 13: realloc invalid values  */
Test(sf_memsuite, realloc_invalid_values, .init = sf_mem_init, .fini = sf_mem_fini) {
	int *val1 = sf_malloc(100);
	val1 = sf_realloc(val1, -100);
	cr_assert(val1 == NULL);

	double *val2 = sf_malloc(80);
	val2 = sf_realloc(val2, -123);
	cr_assert(val2 == NULL);

	long *val3 = sf_malloc(64);
	val3 = sf_realloc(val3, 0);
	cr_assert(val3 == NULL);

	int *val4 = sf_malloc(4096);
	val4 = sf_realloc(val4, 4*4096);
	cr_assert(val4 == NULL);

	int *val5 = sf_malloc(4096);
	val5 = sf_realloc(val5, 12234453452);
	cr_assert(val5 == NULL);
}

/* Test case 13: free invalid values  */
Test(sf_memsuite, free_invalid_values, .init = sf_mem_init, .fini = sf_mem_fini) {
	void *val1 = sf_malloc(4080);
	/* freeing a header, address of payload should've been given */
	sf_free(val1 - 8);
	/* free should fail, alloc bit should still be 1*/
	sf_blockprint(val1-8);
	cr_assert(((sf_header*)(val1-8))->alloc == 1);
	cr_assert(((sf_footer*)(val1+4080))->alloc == 1);

	
	/* modify value of header */
	void *val2 = sf_malloc(64);
	*((long *)(val2-8)) = 0xbeef;
	sf_free(val2);
	/* assert the footer's alloc field is 1, because header has been modified
	   if it hasnt been modified, both the header and footer's alloc would be 0*/
	cr_assert(((sf_footer*)(val2+64))->alloc == 1);
}

/* Test case 14: validate frees and allocations count after reallocation */
Test(sf_memsuite, frees_allocations_count_after_realloc, .init = sf_mem_init, .fini = sf_mem_fini) {
	info meminfo;
	void *val1 = sf_malloc(250);
	sf_info(&meminfo);
	/* assert allocation is 1, free is 0*/
	cr_assert(meminfo.allocations == 1);
	cr_assert(meminfo.frees == 0);

	/* reallocate, shrink(causes splinter) -> allocation == 1, free == 0 */
	val1 = sf_realloc(val1, 248);
	/*update meminfo*/
	sf_info(&meminfo);
	cr_assert(meminfo.allocations == 1);
	cr_assert(meminfo.frees == 0);


	/* reallocate, shrink(DOES NOT cause splinter) -> allocation == 2, free == 1 */
	val1 = sf_realloc(val1, 30);
	/*update meminfo*/
	sf_info(&meminfo);
	cr_assert(meminfo.allocations == 2);
	cr_assert(meminfo.frees == 1);

	/* reallocate to expand, right side has enough memory */
	val1 = sf_realloc(val1, 65);
	/*update meminfo*/
	sf_info(&meminfo);
	cr_assert(meminfo.allocations == 3);
	/* the old block stays */
	cr_assert(meminfo.frees == 1);

	/* allocate and free to create a gap */
	void *val2 = sf_malloc(32);
	sf_free(val2);
	/* use val2 to satisfy the compiler */
	cr_assert(((sf_header*)(val2-8))->alloc == 0);
	/* reallocate to expand, right side DOES NOT have enough memory */
	val1 = sf_realloc(val1, 1000);
	/*update meminfo*/
	sf_info(&meminfo);
	cr_assert(meminfo.allocations == 4);
	/* the old block is freed */
	cr_assert(meminfo.frees == 2);
}
