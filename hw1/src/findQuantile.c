#include "../include/map_reduce.h"
/**
 * Finds percentile in a set of data given
 * @param percentile	quantile we desired, [0.25, 0.5, 0.75]
 * @param n				total number count
 * @param res			stats struct containing info needed 
*/
double findQuantile(double percentile, int n, Stats res) {
	int i, j;
	double quantile = 0.0;
	
	if(n*percentile == (int)(n*percentile)) {
		int a = (int)(n*percentile);
		for(i = 0; i < NVAL; i++) {
			if(res.histogram[i] > a) {
				quantile = i;
				break;
			} else if(res.histogram[i] == a) {
				for(j = i+1; j < NVAL; j++) {
					if(res.histogram[j] > 0) {
						quantile = (i+j)/2.0;
						break;
					}
				}
			} else {
				a = a - res.histogram[i];
			}
		}
	} else {
		int a = (int)(n*percentile) + 1;
		for(i = 0; i < NVAL; i++) {
			if(res.histogram[i] >= a) {
				quantile = i;
				break;
			} else {
				a = a - res.histogram[i];
			}
		}
	}
	
	return quantile;
}
