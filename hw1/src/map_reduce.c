//**DO NOT** CHANGE THE PROTOTYPES FOR THE FUNCTIONS GIVEN TO YOU. WE TEST EACH
//FUNCTION INDEPENDENTLY WITH OUR OWN MAIN PROGRAM.
#include "../include/map_reduce.h"

//Implement map_reduce.h functions here.

/**
 * Validates the command line arguments passed in by the user.
 * @param  argc The number of arguments.
 * @param  argv The arguments.
 * @return      Returns -1 if arguments are invalid (refer to hw document).
 *              Returns 0 if -h optional flag is selected. Returns 1 if analysis
 *              is chosen. Returns 2 if stats is chosen. If the -v optional flag
 *              has been selected, validateargs returns 3 if analysis
 *              is chosen and 4 if stats is chosen.
 */
int validateargs(int argc, char** argv) {

	
	// make sure "./mapreduce" is the first argument, do nothing if it is not
	// because the terminal will provide feedback then
	if(strcmp(argv[0], "./mapreduce")==0) {
		// print the usage when help correctly is requested, return 0
		if(argc == 1) {
			//printf("%s", usage);
			return -1;
		}
		else if(strcmp(argv[1], "-h")==0) {
			//printf("%s", usage);
			return 0;
		} 
		// If the -v optional flag has been selected, 
		else if(strcmp(argv[1], "-v")==0) {
			// if correct number of argument is passed in 
			if(argc == 4){
				// return 3 if analysis is chosen 
				if(strcmp(argv[2], "ana")==0 
				&& fopen(argv[argc - 1], "r")!=NULL
				&& strcmp(argv[argc - 1], ".")!=0
				&& strcmp(argv[argc - 1], "..")!=0
				// && strcmp(argv[argc - 1], "./")!=0
				// && strcmp(argv[argc - 1], "../")!=0
				) {
					return 3;
				} 
				// return 4 if stats is chosen
				else if(strcmp(argv[2], "stats")==0
				&& fopen(argv[argc - 1], "r")!=NULL
				&& strcmp(argv[argc - 1], ".")!=0
				&& strcmp(argv[argc - 1], "..")!=0
				// && strcmp(argv[argc - 1], "./")!=0
				// && strcmp(argv[argc - 1], "../")!=0
				) {
					return 4;
				}  else {
					//printf("%s", usage);
					return -1;
				}
			} else {
				//printf("%s", usage);
				return -1;
			}
		} 
		else if(argc == 3) {

			if (strcmp(argv[1],"ana") == 0
				&& fopen(argv[argc - 1], "r")!=NULL
				&& strcmp(argv[argc - 1], ".")!=0
				&& strcmp(argv[argc - 1], "..")!=0
				// && strcmp(argv[argc - 1], "./")!=0
				// && strcmp(argv[argc - 1], "../")!=0
				) {
				return 1;
			} else if(strcmp(argv[1],"stats") == 0
				&& fopen(argv[argc - 1], "r")!=NULL
				&& strcmp(argv[argc - 1], ".")!=0
				&& strcmp(argv[argc - 1], "..")!=0
				// && strcmp(argv[argc - 1], "./")!=0
				// && strcmp(argv[argc - 1], "../")!=0
				) {
				return 2;
			} else {
				//printf("%s", usage);
				return -1;
			}
		}
		
		// print the usage when argument(s) passed in is incorrect
		else {
			//printf("%s", usage);
			return -1;
		}
		
	} else {
		return -1;
	}

}

/**
 * Counts the number of files in a directory EXCLUDING . and ..
 * @param  dir The directory for which number of files is desired.
 * @return     The number of files in the directory EXCLUDING . and ..
 *             If nfiles returns 0, then print "No files present in the
 *             directory." and the program should return EXIT_SUCCESS.
 *             Returns -1 if any sort of failure or error occurs.
 */
int nfiles(char* dir) {
	
	// make sure no . and .. passed in in the first place
	if (strcmp(dir, ".")!=0 && strcmp(dir, "..")!=0 
	&& strcmp(dir, "./")!=0 && strcmp(dir, "../")!=0) {
		int dir_counter;
		DIR *dir_pt = opendir(dir);	// open the directory
		struct dirent *direct_pt;
		if(dir_pt != NULL) {
			// count the number of files
			dir_counter = 0;
			while((direct_pt=readdir(dir_pt)) != NULL) {
				// excluding . and ..
				if(strcmp(direct_pt->d_name,".") !=0 && strcmp(direct_pt->d_name,"..") !=0) {
					dir_counter++;
				}
			}
			if(dir_counter == 0) {
				printf("No files present in the directory.\n");
				// close the directory before returning value
				closedir(dir_pt);
				return 0;
			} else if(dir_counter > 0) {
				// close the directory before returning value
				closedir(dir_pt);
				return dir_counter;
			} else {
				return -1;
			}
			
		} else {
			// ERROR
			return -1;
		}
	} else {
		return -1;
	}
	
}

/**
 * The map function goes through each file in a directory, performs some action on
 * the file and then stores the result.
 *
 * @param  dir     The directory that was specified by the user.
 * @param  results The space where map can store the result for each file.
 * @param  size    The size of struct containing result data for each file.
 * @param  act     The action (function map will call) that map will perform on
 *                 each file. Its argument f is the file stream for the specific
 *                 file. act assumes the filestream is valid, hence, map should
 *                 make sure of it. Its argument res is the space for it to store
 *                 the result for that particular file. Its argument fn is a
 *                 string describing the filename. On failure returns -1, on
 *                 sucess returns value specified in description for the act
 *                 function.
 *
 * @return        The map function returns -1 on failure, sum of act results on
 *                success.
 */
int map(char* dir, void* results, size_t size, int (*act)(FILE* f, void* res, char* fn)) {
	
	int sumOfActResults = -1;	// for storing return value
	DIR *dir_pt;
	int i = 0;	// counter
	// initialize a chunk of memory for results array
	results = memset(results, 0, sizeof(results)*NFILES);

	if((dir_pt = opendir(dir)) != NULL) {	// proceed if no error
		sumOfActResults = 0;
		struct dirent *direct_pt = NULL;

		if(dir_pt != NULL) {
			// go through each entry in dir
			while((direct_pt = readdir(dir_pt)) != NULL) {
				// make sure . and .. are excluded
				if(strcmp(direct_pt->d_name,".") !=0 && strcmp(direct_pt->d_name,"..") !=0) {
					char *filename = strdup(direct_pt->d_name);
					char *file_path = malloc((strlen(dir) + 1 + strlen(filename) + 1)*sizeof(char));
					strcpy(file_path,dir);
					strcat(file_path,"/");
					strcat(file_path, filename);
					FILE *fp = fopen(file_path, "r");
					if(fp != NULL) {
						
						int actResult = act(fp, results+(size*i), filename);
						if(actResult == -1) {
							sumOfActResults = -1;
							break;
						} else {
							sumOfActResults += actResult;
						}
						fclose(fp);
						free(filename);
						
					} else {
						sumOfActResults = -1;
						break;
					}
					i++;
				}
			}
		} else {
			sumOfActResults = -1;
		}
	} else {
		sumOfActResults = -1;
	}
	return sumOfActResults;
}

/**
 * This reduce function takes the results produced by map and cumulates all
 * the data to give one final Analysis struct. Final struct should contain 
 * filename of file which has longest line.
 *
 * @param  n       The number of files analyzed.
 * @param  results The results array that has been populated by map.
 * @return         The struct containing all the cumulated data.
 */
struct Analysis analysis_reduce(int n, void* results) {

	int i = 0, j = 0;					// loop counters
	struct Analysis cumulated_struct;	// setup return value
	size_t size = sizeof(struct Analysis);

	// inititalize values in cumulated_struct
	for(i = 0; i < 128; i++) { cumulated_struct.ascii[i] = 0;}
	cumulated_struct.lnlen = 0;
	cumulated_struct.lnno = 0;

	for (i = 0; i < n; i++){
		// cast results array to struct Analysis array

		struct  Analysis *anas = ((struct Analysis *)(results + size*i));
		
		// 1. add all the ascii to cumulated_struct
		for (j = 0; j < 128; j++){
			cumulated_struct.ascii[j] += anas->ascii[j];
		}
		// 2. update longest line's length, number and 
		// 		its corresponding filename for cumulated_struct
		if(anas->lnlen >= cumulated_struct.lnlen) {
			cumulated_struct.lnlen = anas->lnlen;
			cumulated_struct.lnno = anas->lnno;
			// 3. update filename: 
			// free -> reallocate? 
			
			cumulated_struct.filename = malloc((strlen(anas->filename)+1)*sizeof(char));
			strcpy(cumulated_struct.filename, anas->filename);
			//free(cumulated_struct.filename);
			//cumulated_struct.filename = strdup(anas->filename);
		}
	}

	return cumulated_struct;
}

/**
 * This reduce function takes the results produced by map and cumulates all
 * the data to give one final Stats struct. Filename field in the final struct 
 * should be set to NULL.
 *
 * @param  n       The number of files analyzed.
 * @param  results The results array that has been populated by map.
 * @return         The struct containing all the cumulated data.
 */
Stats stats_reduce(int n, void* results) {

	int i = 0, j = 0;		// loop counters
	Stats cumulated_stats;	// setup return value
	size_t size = sizeof(Stats);

	// inititalize values to 0 in histogram[NVAL]
	for(i = 0; i < NVAL; i++) { cumulated_stats.histogram[i] = 0;}
	cumulated_stats.sum = 0;
	cumulated_stats.n = 0;
	//set file name to null
	cumulated_stats.filename = NULL;
	//memset(cumulated_stats.filename,0,(strlen(cumulated_stats.filename)+1)*sizeof(char));

	for(i = 0; i < n; i++) {
		 
		Stats *stat = (Stats *)(results+(size*i));
		
		// cumulate data
		for(j = 0; j < NVAL; j++) { 
			cumulated_stats.histogram[j] += (stat->histogram[j]);
		}
		cumulated_stats.sum += stat->sum;
		cumulated_stats.n += stat->n;
	}

	return cumulated_stats;
}

/**
 * Always prints the following:
 * - The name of the file (for the final result the file with the longest line)
 * - The longest line in the directory's length.
 * - The longest line in the directory's line number.
 *
 * Prints only for the final result:
 * - The total number of bytes in the directory.
 *
 * If the hist parameter is non-zero print the histogram of ASCII character
 * occurrences. When printing out details for each file (i.e the -v option was
 * selected) you MUST NOT print the histogram. However, it MUST be printed for
 * the final result.
 *
 * Look at sample output for examples of how this should be print. You have to
 * match the sample output for full credit.
 *
 * @param res    The final result returned by analysis_reduce
 * @param nbytes The number of bytes in the directory.
 * @param hist   If this is non-zero, prints additional information. (Only non-
 *               zero for printing the final result.)
 */
void analysis_print(struct Analysis res, int nbytes, int hist) {
	
	printf("File: %s\n", res.filename);
	printf("Longest line length: %d\n", res.lnlen);
	printf("Longest line number: %d\n", res.lnno);

	int i, j;

	// int total_bytes = 0;
	// for(i = 0; i < 128; i++) {total_bytes += res.ascii[i];}
	// total_bytes = total_bytes * sizeof(char);
	// printf("Total Bytes in directory: %d\n", total_bytes);

	if(hist != 0) {
		printf("Total Bytes in directory: %d\n", nbytes);
		if(nbytes > 0) {
			printf("Histogram:\n");
			for(i = 0; i < 128; i++) {
				if(res.ascii[i] > 0) {
					printf(" %d:", i);
					// print histogram
					for(j = 0; j < res.ascii[i]; j++) {
						putchar('-');
					}
					putchar('\n');
				}
			}
		}
	}
}

/**
 * Always prints the following:
 * Count (total number of numbers read), Mean, Mode, Median, Q1, Q3, Min, Max
 *
 * Prints only for each Map result:
 * The file name
 *
 * If the hist parameter is non-zero print the the histogram. When printing out
 * details for each file (i.e the -v option was selected) you MUST NOT print the
 * histogram. However, it MUST be printed for the final result.
 *
 * Look at sample output for examples of how this should be print. You have to
 * match the sample output for full credit.
 *
 * @param res  The final result returned by stats_reduce
 * @param hist If this is non-zero, prints additional information. (Only non-
 *             zero for printing the final result.)
 */
void stats_print(Stats res, int hist) {
	int i, j;
	double mean =0.0, median = 0.0, Q1 = 0, Q3 = 0;
	int mode = 0;
	int mode_index = 0;
	int min = 0, max = 0;
	
	if (hist != 0){
		if(res.n > 0) {
			printf("Histogram:\n");
			
			for(i = 0; i < NVAL; i++) {
				if(res.histogram[i] > 0) {
					printf("%d  :", i);		// number with two spaces before colon
					for(j = 0; j < res.histogram[i]; j++) {
						putchar('-');
					}
					putchar('\n');
				}
			}
			putchar('\n');
		}
	}
	// print filename if 
	if(hist == 0) {
		printf("File: %s\n", res.filename);
	}

	printf("Count: %d\n", res.n);

	// find and print mean 
	mean = (double)res.sum/(double)res.n;
	printf("Mean: %.6f\n", mean);

	// find and print mode
	printf("Mode:");
	for(i = 0; i < NVAL; i ++) {
		if(res.histogram[i] > mode) {
			mode = res.histogram[i];
			mode_index = i;
		}
	}
	printf(" %d", mode_index);
	// check for more mode (i.e. numeric value w/same number of count as mode) 
	//					   (after mode)
	//if(mode_index + 1 != NVAL) {
		for(i = mode_index + 1; i < NVAL; i++) {
			if(res.histogram[i] == mode) {
				mode_index = i;
				printf(" %d", mode_index);
			}
		}
	//}
	putchar('\n');

	// find and print median
	median = findQuantile(0.5, res.n, res);
	printf("Median: %.6f\n", median);

	// find and print Q1
	Q1 = findQuantile(0.25, res.n, res);
	printf("Q1: %.6f\n", Q1);

	// find and print Median
	Q3 = findQuantile(0.75, res.n, res);
	printf("Q3: %.6f\n", Q3);

	// find and print min
	for(i = 0; i < NVAL; i++) {
		if(res.histogram[i] > 0) {
			min = i;
			break;
		}
	}
	printf("Min: %d\n", min);

	// find and print max
	for(i = NVAL - 1; i >= 0; i--) {
		if(res.histogram[i] > 0) {
			max = i;
			break;
		}
	}
	printf("Max: %d\n", max);
}

/**
 * This function performs various different analyses on a file. It
 * calculates the total number of bytes in the file, stores the longest line
 * length and the line number, and frequencies of ASCII characters in the file.
 *
 * @param  f        The filestream on which the action will be performed. You
 *                  you can assume the filestream passed by map will be valid.
 * @param  res      The slot in the results array in which the data will be
 *                  stored.
 * @param  filename The filename of the file currently being processed.
 * @return          Return the number of bytes read.
 */
int analysis(FILE* f, void* res, char* filename) {
	
	int bytes_read = 0;
	char c;

	struct Analysis *ana = (struct Analysis*)res;
	ana->filename = malloc((strlen(filename)+1)*sizeof(char));
	strcpy(ana->filename,filename);

	int counter = 0;	// line character counter
	int line_counter = 1;
	int lnno = 0;
	int lnlen = 0;

	while((c=fgetc(f)) != EOF) {
		
		counter++;
		bytes_read += (sizeof(char));	// increament bytes read
		// read a whole line
		// count the total number of character in the line, 
		// save line number to lnno and its number of character to lnlen
		// IF the line that's being read has a larger lnlen
		if((int)c < 128 && (int)c >= 0) {
			// increament count of each ascii read
			// make sure the character read is within range of array
			ana->ascii[(int)c] = ana->ascii[(int)c]+1;
		}

		if(c=='\n') {
			if(counter >= lnlen) {
				lnlen = counter - 1;// new lnlen, -1 because a \n is being read
				lnno = line_counter;// new lnno
				counter = 0;		// reset counter
			} else {
				counter = 0;
			}
			line_counter++;			// update line counter when reaches NL
		}

	} 
	// do not miss EOF
	if(counter >= lnlen) {
		lnlen = counter;// new lnlen, didnt -1 because a EOF is being read
		lnno = line_counter;// new lnno
		//counter = 0;		// reset counter
	}

	// store final info
	ana->lnlen = lnlen;
	ana->lnno = lnno;

	return bytes_read;
}

/**
 * This function counts the number of occurrences of each number in a file. It
 * also calculates the sum total of all numbers in the file and how many numbers
 * are in the file. If the file has an invalid entry return -1.
 *
 * @param  f        The filestream on which the action will be performed. You
 *                  you can assume the filestream passed by map will be valid.
 * @param  res      The slot in the results array in which the data will be
 *                  stored.
 * @param  filename The filename of the file currently being processed.
 * @return          Return 0 on success and -1 on failure.
 */
int stats(FILE* f, void* res, char* filename) {
	Stats *stat = (Stats*)res;
	stat->filename = malloc((strlen(filename)+1)*sizeof(char));
	strcpy(stat->filename,filename);	// store filename

	int i;
	// initialize array
	for(i = 0; i < NVAL; i++) { stat->histogram[i] = 0;}

	int sum = 0;	// for storing sum
	int n = 0;		// for storing number count
	int num;		// number read in
	int returnVal;	// return value

	while(1) {
		// read one number at a time (up till space)
		// n++, sum+=num, add num to histogram array IF a num a read in 
		if (fscanf(f, "%d", &num) == 1) {
			
			if(num>=0 && num<NVAL) {
				n++;
				sum += num;
				(stat->histogram[num]) = (stat->histogram[num])+1;
			} else {
				returnVal = -1;
				break;
			}
		}

		// return 0 and exit when EOF is reached
		// return 1 and exit when error occures
		if(feof(f)) {
			returnVal = 0;
			break;
		} else if(ferror(f)) {
			returnVal = -1;
			break;
		}
	}
	stat->n = n;
	stat->sum = sum;
	return returnVal;
}