#include "../include/map_reduce.h"

//Space to store the results for analysis map
struct Analysis analysis_space[NFILES];
//Space to store the results for stats map
Stats stats_space[NFILES];

//Sample Map function action: Print file contents to stdout and returns the number bytes in the file.
int cat(FILE* f, void* res, char* filename) {
    char c;
    int n = 0;
    printf("%s\n", filename);
    while((c = fgetc(f)) != EOF) {
        printf("%c", c);
        n++;
    }
    printf("\n");
    return n;
}

int main(int argc, char** argv) {
    const char *usage = "Usage:\t.\\mapreduce [h|v] FUNC DIR\n\tFUNC\tWhich operation you would like to run on the data:\n\t\tana - Analysis of various text files in a directory.\n\t\tstats - Calculates stats on files which contain only numbers.\n\tDIR\tThe directory in which the files are located.\n\t\tOptions:\n\t-h\tPrints this help menu.\n\t-v\tPrints the map function's results, stating the file it's from.\n";

    
    int returnVal = EXIT_SUCCESS;
    
	int validateCode = validateargs(argc, argv); // return value of validateargs(argc, argv)
	int n;
	if(validateCode >= 1 && validateCode <= 4) {
		n = nfiles(argv[argc - 1]);	// number of file being accessed
	}

	if(validateCode == 0) {
		printf("%s", usage);
		returnVal = EXIT_SUCCESS;
	} else if (validateCode == 1) {
		// just map with ana
		if (n > 0) {
			int nbytes = map(argv[argc - 1], analysis_space, sizeof(struct Analysis), analysis);
			if(nbytes == -1) {
				printf("%s", usage);
				returnVal = EXIT_FAILURE;
			} else {
				struct Analysis ana = analysis_reduce(n, analysis_space);
				analysis_print(ana, nbytes, 1);
			}
			// returnVal = EXIT_SUCCESS;			
		} else if(n == 0) {
			// exit success
			// returnVal = EXIT_SUCCESS;
		} else {
			// error
			printf("%s", usage);
			returnVal = EXIT_FAILURE;
		}

	} else if(validateCode == 2) {
		// just map with stats
		if (n > 0) {
			int rtVal = map(argv[argc - 1], stats_space, sizeof(Stats), stats);	// map
			if(rtVal == -1) {
				printf("%s", usage);
				returnVal = EXIT_FAILURE;
			} else {
				Stats stats_result = stats_reduce(n, stats_space);		// reduce
				stats_print(stats_result, stats_result.n);				// print
				// returnVal = EXIT_SUCCESS;
			}
			
		} else if(n == 0) {
			// exit success
			// returnVal = EXIT_SUCCESS;
		} else {
			// error
			printf("%s", usage);
			returnVal = EXIT_FAILURE;
		}
	} else if(validateCode == 3) {
		
		if (n > 0) {
			int nbytes = map(argv[argc - 1], analysis_space, sizeof(struct Analysis), analysis);
			if(nbytes == -1) {
				printf("%s", usage);
				returnVal = EXIT_FAILURE;
			} else {
				struct Analysis ana = analysis_reduce(n, analysis_space);

				int i;
				for(i = 0; i < n; i++) {
					struct Analysis ana1 = analysis_space[i];
					// 0 for 2nd param is for no letting bytes to show
					analysis_print(ana1, 0, 0);
					putchar('\n');
				}

				analysis_print(ana, nbytes, ana.lnlen);
				// returnVal = EXIT_SUCCESS;
			}
		} else if(n == 0) {
			// exit success
			// returnVal = EXIT_SUCCESS;
		} else {
			// error
			printf("%s", usage);
			returnVal = EXIT_FAILURE;
		}
		
	} else if(validateCode == 4) {
		// print each file and final stat
		if (n > 0) {
			int rtVal = map(argv[argc - 1], stats_space, sizeof(Stats), stats);
			if(rtVal == -1) {
				printf("%s", usage);
				returnVal = EXIT_FAILURE;
			} else {
				Stats stats_result = stats_reduce(n, stats_space);
				int i;
				for(i = 0; i < n; i++) {
					stats_print(stats_space[i], 0);
					putchar('\n');
				}

				stats_print(stats_result, 1);
			}
			// returnVal = EXIT_SUCCESS;
		} else if(n == 0) {
			// exit success
			// returnVal = EXIT_SUCCESS;
		} else {
			// error
			printf("%s", usage);
			returnVal = EXIT_FAILURE;
		}
	} else {
		printf("%s", usage);
		returnVal = EXIT_FAILURE;
	}
    
	return returnVal; 
}
