#include "sfish.h"
#include "functions.h"
#include "redirection.h"
#include "part2.h"
#include "jobctrl.h"
#include "extra_credit1.h"

char *cmd_readline;          /* full cmd from termnal */
int runInBg;
char *currentFGJob;
int main(int argc, char** argv) {
    //DO NOT MODIFY THIS. If you do you will get a ZERO.
    rl_catch_signals = 0;
    //This is disable readline's default signal handlers, since you are going
    //to install your own.

    
    char **cmdTokens;   /* parsed cmd tokens */
    int numTokens;      /* keep track of number of tokens, for freeing later */
    int i;              /* loop  counter */
    int rtval_check_quote;
    /* initialize shell*/
    initShell();

    /* start the shell */
    while((cmd_readline = readline(prompt)) != NULL) {

        //TODO: check job list and rm any finished jobs 
        updateJobPool();
        updateFG_PIDList();
        updatePrtVal();

        if((rtval_check_quote=isQuotedArgBalanced(cmd_readline))!=1) {
            // need to balance the quote 
            balanceQuote(cmd_readline, rtval_check_quote);
        }

        cmdTokens = malloc(sizeof(char*) * strlen(cmd_readline));

        /* if the cmd_readline entered is not an empty string, then parse and handle cmd_readline */
        if(strlen(cmd_readline) != 0) {

            // cmd attempted
            num_cmd_ran++;
            
            int redirect = 0;
            int numPipes = 0;
            runInBg = 0;
            /* tokenize cmd_readline, it returns number of pipes */
            numPipes=tokenizeCMD(cmd_readline, cmdTokens, &numTokens, &redirect, &runInBg);
            if(runInBg==1) {
                handle_bg_cmd(cmd_readline, cmdTokens, numTokens, redirect, numPipes);
            } else {

                // make a copy of current cmd
                currentFGJob = malloc(sizeof(char) * strlen(cmd_readline) + 1);
                strcpy(currentFGJob, cmd_readline);
    
                if(numPipes > 0) {
                    /* if there's pipe, handle each process in pipe separately */
                    handleRedirection_PIPE(cmd_readline, cmdTokens, numTokens, numPipes);
                } else {
                    /* identify and handle cmd_readline */
                    handleCMD(cmd_readline, cmdTokens, numTokens, redirect);
                    //printf("%d\n", getpid());
                }

                free(currentFGJob);
            }
            
            /* free tokens for each loop */
            
            for(i=0; i<numTokens; i++) {
                // printf("%s\n", cmdTokens[i]);
                if(cmdTokens[i] != NULL ) {
                    free(cmdTokens[i]);
                }
            }
            numTokens=0;
            
        }
        free(cmdTokens);
        free(cmd_readline);
    }

    return EXIT_SUCCESS;
}


/**
 * Determines whether the argument is balanced with its quote
 */
int isQuotedArgBalanced(char *cmd) {
    /* go thru the entire cmd to determine if the cmd is balanced */
    int i = 0;
    int squoteFlag = 1; // 1 is balanced -1 is unbalanced
    int dquoteFlag = 2; // 2 is balanced -2 is unbalanced
      
    while(cmd[i]!='\0') {
        if(cmd[i]=='\'') {
            squoteFlag*=(-1);
            i++;
            /* look for the next single quote
               ignore double quotes in between
            */
            while(cmd[i]!='\0') {
                if(cmd[i]=='\'' && cmd[i-1] != '\\') {
                    squoteFlag*=(-1);
                    i++;
                    break;
                } else {
                    i++;
                }
            }
        } else if(cmd[i]=='\"') {
            dquoteFlag*=(-1);
            i++;
            /* look for the next double quote
               ignore single quotes in between
            */
            while(cmd[i]!='\0') {
                if(cmd[i]=='\"' && cmd[i-1] != '\\') {
                    dquoteFlag*=(-1);
                    i++;
                    break;
                } else {
                    i++;
                }
            }
        } else {
            i++;
        }
    }
    if(squoteFlag == -1) {
        return -1;
    } else if(dquoteFlag == -2) {
        return -2;
    } else {
        return 1;
    }
}

int findQuoteIndex(char *cmd, int quote) {
    int index = -1;
    int i = 0;
    if(quote == -1) {
        // look for single quote
        while(cmd[i]!='\0') {
            if(cmd[i]=='\'') {
                index = i;
                break;
            } else {
                i++;
            }
        }
    } else {
        // look for double quote
        while(cmd[i]!='\0') {
// printf("cmd[i] = %d\n", cmd[i]);
            if(cmd[i]=='\"') {
                index = i;
                break;
            } else {
                i++;
            }
        }
    }
    return index;
}

void balanceQuote(char *cmd_readline, int target_quote) {
// printf("target_quote: %d\n", target_quote);
    char *cmd;
    int index;
    while((cmd = readline(">")) != NULL) {
// printf("findQuoteIndex(cmd,target_quote) = %d\n", findQuoteIndex(cmd,target_quote));
        if((index=findQuoteIndex(cmd,target_quote)!=-1)) {
            // found matching quote check if later portion is balanced
            if((target_quote = isQuotedArgBalanced(cmd+index+1))==1) {
                cmd_readline = realloc(cmd_readline, strlen(cmd_readline)+strlen(cmd)+1);
                strcat(cmd_readline,cmd);
                free(cmd);
                break;
            }
        } 
        
        cmd_readline = realloc(cmd_readline, strlen(cmd_readline)+strlen(cmd)+1);
        strcat(cmd_readline,cmd);
        free(cmd);  
    }
}


