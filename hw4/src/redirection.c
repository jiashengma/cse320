#include "sfish.h"
#include "redirection.h"
#include "part2.h"
#include "functions.h"
#include "jobctrl.h"


/**
 * @param prog: first cmd, the path of the program to be executed
 */
int handleRedirection_NOPIPE(char *fullCMD, char **cmdTokens, int numTokens) {
    int i; // counter
    int retval = 0;
    int out_fd = -1; // output file descriptor
    int in_fd = -1; // input file descriptor
    int err_fd = -1;
    
    pid_t pid;
    int child_status;
    int err_flag = 0;
    
    int k = 1; // index tracker: for current token of the new cmd(without redirection operator and file )

    /* get all the redirection operators */
    for(i = 1; i < numTokens-1; i++) {
        /**********************************************/
        /* look for < */
        if(strcmp(cmdTokens[i],"<")==0) {
            
            if(in_fd!=-1) {
                /* close the prev one, we dont need it */
                close(in_fd);
            }
            if(cmdTokens[i+1]!=NULL
               && strcmp(cmdTokens[i+1], "<")!=0
               && strcmp(cmdTokens[i+1], ">")!=0
               && strcmp(cmdTokens[i+1], "2>")!=0) {
                
                in_fd = open(cmdTokens[i+1], O_RDONLY);
                
                if(in_fd == -1) {
                    fprintf(stderr, "%s: No such file or directory\n", cmdTokens[i+1]);
                    err_flag = 1;
                    break;
                }
            } else {
                //FIXME: error can also be : syntax error near unexpected token `>'
                //       if cmd is: cmd > >
                fprintf(stderr, "sfish: syntax error near unexpected token 'newline'\n");
                err_flag = 1;
                break;
            }
            
            /* set < and file after it to be empty space */
//            cmdTokens[i] = cmdTokens[i+1] = " ";
            i++; // skip the next token
        }
        
        /**********************************************/
        /* look for > */
        else if(strcmp(cmdTokens[i],">")==0) {
            /* create output file */
            if(out_fd!=-1) {
                /* close the prev one, we dont need it */
                close(out_fd);
            }
            
            if(cmdTokens[i+1]!=NULL
               && strcmp(cmdTokens[i+1], "<")!=0
               && strcmp(cmdTokens[i+1], ">")!=0
               && strcmp(cmdTokens[i+1], "2>")!=0) {
                out_fd = open(cmdTokens[i+1], O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
            } else {
                //FIXME: error can also be : syntax error near unexpected token `>'
                //       if cmd is: cmd > >
                fprintf(stderr, "sfish: syntax error near unexpected token 'newline'\n");
                err_flag = 1;
                break;
            }
            /* set < and file after it to be empty space */
//            cmdTokens[i] = cmdTokens[i+1] = " ";
            i++; // skip the next token
        }
        
        /**********************************************/
        /* look for 2> */
        else if(strcmp(cmdTokens[i],"2>")==0) {
            // dup2(,2)
            /* create output file */
            if(err_fd!=-1) {
                /* close the prev one, we dont need it */
                close(err_fd);
            }
            
            if(cmdTokens[i+1]!=NULL
               && strcmp(cmdTokens[i+1], "<")!=0
               && strcmp(cmdTokens[i+1], ">")!=0
               && strcmp(cmdTokens[i+1], "2>")!=0) {
                err_fd = open(cmdTokens[i+1], O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
            } else {
                //FIXME: error can also be : syntax error near unexpected token `>'
                //       if cmd is: cmd > >
                fprintf(stderr, "sfish: syntax error near unexpected token 'newline'\n");
                err_flag = 1;
                break;
            }
            /* set < and file after it to be empty space */
//            cmdTokens[i] = cmdTokens[i+1] = " ";
            i++; // skip the next token
        } else {
            cmdTokens[k] = cmdTokens[i];
            k++;
        }
    } /* end of getting all the redirection operators */
    cmdTokens[k] = NULL;
    
    /* handle CMD */
    if(err_flag != 1){
        
        /* execv */
        pid = fork();
        if(pid == 0) {
            /* redirect by calling dup */
            /* in */
            if(in_fd != -1) {
                // dup2, then exec
                dup2(in_fd,0);
                close(in_fd);
            }
            /* out */
            if(out_fd != -1) {
                dup2(out_fd,1);
                close(out_fd);
            }
            /* err */
            if(err_fd != -1) {
                dup2(err_fd,2);
                close(err_fd);
            }
            
            
            /* execv non-buildin, handle buildin */
            if(isBuildin(cmdTokens[0])) {
                // handle buildin
                handleBuildin(cmdTokens, numTokens);
            } else {
                // handle non buildin
                handleNonBuildin(cmdTokens, numTokens);
            }
            /* exits current child process */
            _exit(EXIT_SUCCESS);
        } else {
        
            addfg_pid(pid);
            /* wait to reap child */
            waitpid(pid, &child_status, 0);
            // rmfg_pid(pid);

            retval = 1;
        }
    } else {
        /* error */
        retval = 0;
    }
    
    return retval;
    
}


int handleRedirection_PIPE(char *fullCMD, char **cmdTokens, int numTokens, int numPipes) {
    int i,j; // counters
    int pipeIndex = 0; // index of pipe
    int retval = 0;

    int pipes[numPipes][2];
    /* create pipes */
    for(i = 0; i < numPipes; i++) {
        pipe(pipes[i]);
    }
    
    pid_t pids[numPipes+1];
    int child_status;
    
    
    /* execute each process */
    for(i = 0; i <= numPipes; i++) {
        int k = 0; // counter of number of tokens in cmd 
        /* allocate mem for char arry ptr */
        char **cmd = malloc(sizeof(char*)*numTokens);
        
        /* There are numPipes+1 processes,
           parse the arguments again to execute each process */
        
        int redirect = 0; // flag to indicate redirection of this cmd
        
        /* separate cmd */
        for(j = pipeIndex; j < numTokens; j++) {
       
            if(strcmp(cmdTokens[j], "|")==0) {
                /* found pipe index, now move on to next cmd */
                pipeIndex = j+1;
                break;
            } else {
                /* raise flag if there's redirection operator */
                if (strcmp(cmdTokens[j], ">")==0
                    || strcmp(cmdTokens[j], "<")==0
                    || strcmp(cmdTokens[j], "2>")==0) {
                    redirect = 1;
                }
                // TODO: remember to free it
                // cmd[k] = malloc(sizeof(char) * strlen(cmdTokens[j]) + 1);
                //strcpy(cmd[k],cmdTokens[j]);
                cmd[k] = cmdTokens[j];
                k++;
            }

        } 
        
        /* add NULL to end of cmd */
        //cmd[k] = malloc(sizeof(char) * strlen(cmdTokens[j]) + 1);
        cmd[k] = (char*)NULL;
        
        
        pids[i] = fork();
        if(pids[i]==0) {
            if(i==0) {
                /* first process does not need a read end */
                dup2(pipes[i][1], STDOUT_FILENO);
                
            } else if (i==numPipes) {
                /* last process does not need a write end */
                dup2(pipes[i-1][0], STDIN_FILENO);
            } else {
                /* need prev's read end and current's write end */
                dup2(pipes[i-1][0], STDIN_FILENO);
                dup2(pipes[i][1], STDOUT_FILENO);
            }
            
            /* close all pipes */
            closeAllPipes(pipes, numPipes);
            
            handleCMD(fullCMD, cmd, k, redirect);

            _exit(EXIT_SUCCESS);
        } else {

            /* parent process, free cmd */
            free(cmd);
        }
    }

    /* close all pipes */
    closeAllPipes(pipes, numPipes);

    /* wait to reap each child */
    for(i = 0; i <= numPipes; i++) {
        waitpid(pids[i],&child_status,0);
    }
    
    return retval;
    
}

void closeAllPipes(int pipes[][2], int numPipes) {
    int i;
    for(i = 0; i < numPipes; i++) {
        close(pipes[i][0]);
        close(pipes[i][1]);
    }
}
