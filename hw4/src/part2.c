#include "part2.h"
#include "sfish.h"
#include "functions.h"
#include "redirection.h"
#include "jobctrl.h"

/* for PATH env variable */    
char **pathTokens;
int numPathTokens;

pid_t handleNonBuildin(char **cmdTokens, int numTokens/*, int runInBg*/) {
	/* if program found, execute it: fork -> exev -> wait for exit */
	char *path = findProgram(cmdTokens[0]);
	pid_t pid = 0 /*, wpid*/;
    int child_status;

    if((pid = fork())==0) {
    	
    	if(path != NULL) {
    		//printf("%s\n", path);
    		if(execv(path, cmdTokens) < 0) {
    			fprintf(stderr, "%s: command not found\n", cmdTokens[0]);
            	_exit(EXIT_FAILURE);
            }
        } else {
			fprintf(stderr, "%s: command not found\n", cmdTokens[0]);
			_exit(EXIT_FAILURE);
		}
		_exit(EXIT_SUCCESS);
    } else {

    	if(runInBg==0) {
	        /* add pid to fg pid */
	        addfg_pid(pid);
	        /* wait to reap child */
	    	waitpid(pid, &child_status, WUNTRACED);
	    	//printf("something\n");

	    } else {
	    	// bg process
	    	pid_t wpid = waitpid(pid, &child_status, WNOHANG|WUNTRACED);
	    	if(wpid == pid) {
	    		printf("Done\n");
	    	} else if(wpid == 0) {
	    		// still running 
	    	} else {
	    		// error
	    		perror(cmdTokens[0]); 
	    	}

	    }
    
        /* store return code */
        rtCode = child_status;
        
        // if(!isBuildin(cmdTokens[0])) {
            free(path);
        // }
        //free pathTokens
  //       int i;
		// for(i=0; i<numPathTokens; i++) {
		// 	free(pathTokens[i]);
		// }
		// free(pathTokens);
    }

	return pid;
}

void tokenizePATH() {
	char *env_path_var = getenv("PATH");
	pathTokens = malloc(sizeof(char*)*strlen(env_path_var));
	int i; /* counter */
	i = 0;
	numPathTokens = 0;

	/* tokenize PATH */
	char *token = strtok(env_path_var, ":");
	while(token!=NULL) {
		/*TODO: free each one of them before exit */
		pathTokens[i] = malloc(strlen(token)+1);
		strcpy(pathTokens[i], token);
		token = strtok(NULL, ":");
		i++;
	} 

	/* store the number of tokens */
	numPathTokens = i;
} 

/** 
 * determines whether program indicated by path exists
 * if so, it returns path, NULL otherwise
 */
char* findProgram(char *path) {
	/* call stat to determine the existence of the program 
		first, call stat to check executables, if it failed,
		then check if the path has "/", if so, call stat with 
		       each token in pathTokens
		       else the path does not have "/", add and call stat
	 */
	struct stat buf;
	if(stat(path, &buf)==0) {
		return strdup(path);
	} else {
		int i;
		for(i=0; i<numPathTokens; i++) {
			/* build a path for stat check */
			char envPath[strlen(pathTokens[i])+1+strlen(path)+1];
			strcpy(envPath, pathTokens[i]);
				
			if(path[0] != '/') {
				/* add "/" to the path if there isnt */
				strcat(envPath, "/");
			}
			strcat(envPath, path);

			if(stat(envPath, &buf)==0) {
				/* use strdup to avoid dangling pointer 
				   but remember to free it */
				return strdup(envPath);
			}
		}
		return NULL;
	}
}
