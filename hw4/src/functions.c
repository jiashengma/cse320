#include "sfish.h"
#include "functions.h"
#include "part2.h"
#include "redirection.h"
#include "jobctrl.h"
#include "extra_credit1.h"

char *pwd;
char *oldpwd;
int rtCode;

/* prompt toggles */
int user;
int machine;
/* bold flags */
int userPmt_B; 
int machinePmt_B;

char user_str[MAX_HOSTNAME];
char machine_str[MAX_HOSTNAME];
char prompt[MAX_PROMPT];
char userColor[COLOR_LENGTH];
char machineColor[COLOR_LENGTH];

/* redirection flag */
int redirect;
int hasPipe;
int numPipes;
JobPool *jobPool;
pid_t fg_pids[MAX_FG_PID];
int fg_pids_counter;

pid_t bg_pgid, fg_pgid; 
int num_cmd_ran;
pid_t spid;

void initShell() {

	/* bind keys to functions */
	rl_bind_keyseq("\\C-H", helpMenu);
	num_cmd_ran = 0;
	rl_bind_keyseq("\\C-P", printInfo);
	spid = -1;
	rl_bind_keyseq("\\C-B", storePID);
	rl_bind_keyseq("\\C-G", getPID);


	/* no cmd yet */
	rtCode = 0;
	// set $?
	setenv("?", "0", 1);

	/* get pwd */
	pwd = malloc(sizeof(char) * MAX_PATH);
	oldpwd = NULL;

	/* initialize job pool */
    jobPool = malloc(sizeof(JobPool));
	// jobPool->head = jobPool->tail = NULL;
	jobPool->jobCounter = 0;

	updatepwd();

	/* user and machine flags should be ON at start */
	user = machine = 1;

	/* get user and host manchine */
	strcpy(user_str, getenv("USER"));
	// TODO: add flag to make sure if error occurred, appropriate action will be taken
	gethostname(machine_str, MAX_HOSTNAME);

	/* set up prompt */
    updatePrompt();
    
	/* no redirection yet */
	redirect = 0;

	/*TODO: install signal handler*/
	installSignalHanlders();

	/* tokenize PATH env for later use */
	tokenizePATH();

	/* initialize fd and bg process group id to shell process */
	fg_pgid = getpid();
	/* set foreground process group */
	setpgid(fg_pgid, fg_pgid);
	tcsetpgrp(STDIN_FILENO, fg_pgid);
	
	bg_pgid = 0;

	/* no fg pid yet */
	int i;
	for(i=0; i<MAX_FG_PID; i++) {fg_pids[i] = 0;}
	fg_pids_counter = 0;
}

/**
 * Handles "help" command
 */
void handle_HELP() {

	pid_t pid/*, wpid*/;
	int child_status;
	if((pid = fork())==0) {
		printf("%s", HELP_MENU);
		printf("%s", HELP_MENU2);
		_exit(EXIT_SUCCESS);
	} else {
		/* wait for child process to terminate then reap*/
		/*wpid = */waitpid(pid, &child_status, WUNTRACED);

		/* store return code */
		//rtCode = child_status;
	}
}

void handle_CD(char **cmdTokens, int numTokens) {
		/* if no specified path, change pwd to HOME */
		if(numTokens == 1) {
			if(chdir(getenv("HOME"))==0) {
				/* If the shell is in the user’s home directory, 
				   the current working directory should be displayed as ~*/
				
				/* update oldpwd */
				if(oldpwd!=NULL){free(oldpwd);}
				oldpwd = strdup(pwd);

				/* update prompt to display correct path */
				updatePrompt();
			} else {
				/* error */
			}
		} else {
			/* change pwd to path given */
			if(strcmp(cmdTokens[1],"-")==0) {
				/* handle cd - 
				use getenv("OLDPWD") to get old pwd */
// #ifdef DEBUG
// if(oldpwd!=NULL)
// printf("%s\n", oldpwd);
// #endif	
				if(oldpwd!=NULL && chdir(oldpwd)==0) {
					/* free the old oldpwd, strdup the old pwd to serve as the new oldpwd*/
					free(oldpwd);
					oldpwd = strdup(pwd);

					/* old pwd will be updated to the new pwd below*/
					updatePrompt();
				} else {
					/* something went wrong */
					fprintf(stderr, "cd: OLDPWD not set\n");
				}

			} else if(chdir(cmdTokens[1])==0) {

				/* update oldpwd */
				if(oldpwd!=NULL){free(oldpwd);}
				oldpwd = strdup(pwd);

				/* update prompt to display correct path */
				updatePrompt();
			} else {
				/* error */
				fprintf(stderr, "cd: %s: No such file or directory\n", cmdTokens[1]);
			}
		}

		/* store return code */
		// rtCode = child_status;
}

/**
 * Handles the pwd buildin - prints the pwd
 */
void handle_PWD() {
	pid_t pid/*, wpid*/;
	int child_status;
	if((pid = fork())==0) {
		
		/* update pwd */
		updatepwd();

		/* print pwd to terminal */
		printf("%s\n", pwd);
		
		/* exit child process */
		exit(EXIT_SUCCESS);
	} else {
		/* wait for child process to terminate then reap */
		/*wpid = */waitpid(pid, &child_status, WUNTRACED);

		/* store return code */
		//rtCode = child_status;
	}
}

void updatepwd() {
	int i; /* counter to keep track of realloc */
	i = 1;
	while(1) {
		/* allocate more space if the pwd has a long path */
		if(getcwd(pwd, i*MAX_PATH)==NULL) {
			i++;
			pwd = realloc(pwd, i*MAX_PATH);
		} else {
			break;
		}
	}

	/* replace home dir with ~ */
	/* if pwd is HOME, then change the path to "~" */
	// if(strcmp(pwd, getenv("HOME")) == 0) {
	// 	strcpy(pwd, "~");
	// } else if(strstr(pwd, getenv("HOME"))!=NULL) {
	// 	char temp[strlen(pwd)];
	// 	strcpy(temp,pwd);

	// 	strcpy(pwd, "~");
	// 	strcat(pwd, temp+strlen(getenv("HOME")));
	// }

	
}

/**
 * Handles the prt buildin - prints the return code of the previous command
 */
void handle_PRT() {
	pid_t pid/*, wpid*/;
	int child_status;
	if((pid = fork())==0) {
		/*TODO: validate */
		printf("%d\n", rtCode);

		// printf("%s\n", getenv("?"));
		
		exit(EXIT_SUCCESS);

	} else {
		waitpid(pid, &child_status, WUNTRACED);

		/* store return code */
		// rtCode = child_status;
	}
	
}

/*Usage: chpmt SETTING TOGGLE*/
void handle_CHPMT(char **cmdTokens, int numTokens) {
	/* flag to indicate error in cmd */
	int flag = 0;

	if(numTokens!=3) {
		/* error */
		flag = 1;
	} else {
		if(strcmp(cmdTokens[1], "user")==0) {
			int toggle = atoi(cmdTokens[2]);
			if(toggle==0 || toggle==1){
				toggleUser(toggle);
			} else {
				/* error */
				flag = 1;
			}

		} else if(strcmp(cmdTokens[1], "machine")==0) {
			int toggle = atoi(cmdTokens[2]);
			if(toggle==0 || toggle==1){
				toggleMachine(toggle);
			} else {
				/* error */
				flag = 1;
			}
		} else {
			/* error */
			flag = 1;
		}

	}

	/* if command is correct, handle cmd accordingly*/
	if(flag != 1) {
		updatePrompt();
	} else {
		/* TODO: flag is up: print usage */
		fprintf(stderr, "Usage: chpmt SETTING TOGGLE\n"
			"\tValid values for SETTING are:\n"
			"\t\tuser : The user field in the prompt.\n"
			"\t\tmachine : The context field in the prompt.\n"
			"\tValid values of TOGGLE are:\n"
			"\t\t1 : Enabled\n"
			"\t\t0 : Disabled\n");

	}

	/* store return code */
	/* rtCode = child_status;*/
}

/* Usage: chclr SETTING COLOR BOLD*/
void handle_CHCLR(char **cmdTokens, int numTokens) {
	/* flag to indicate error in cmd */
	int flag = 0;

	if(numTokens == 4) {
		char *setting = cmdTokens[1];
		char *color = cmdTokens[2];
		int bold = atoi(cmdTokens[3]);

		if(strcmp(setting, "user")==0) {

			if(bold==0 || bold==1) {
				toggleUserPmt_B(bold);
				setcolor(cmdTokens[3],color,0);
				updatePrompt();

			} else {
				/* error */
				flag = 1;
			}
			
		} else if(strcmp(setting, "machine")==0) {
			if(bold==0 || bold==1) {
				toggleMachinePmt_B(bold);
				setcolor(cmdTokens[3],color,1);
				updatePrompt();
				
			} else {
				/* error */
				flag = 1;
			}
		} else {
			flag = 1;
		}

	} else {
		flag = 1;
	}

	/* if command is correct, handle cmd accordingly*/
	if(flag != 1) {
		updatePrompt();
	} else {
		/* TODO: flag is up: print usage */
		//TODO: print error msg
		fprintf(stderr, "Usage: chclr SETTING COLOR BOLD\n"
			"\tValid values for SETTING are:\n"
				"\t\tuser : The user field in the prompt.\n"
				"\t\tmachine : The context field in the prompt.\n"
			"\tValid values for COLOR are:\n"
				"\t\tred : ANSI red.\n"
				"\t\tblue : ANSI blue.\n"
				"\t\tgreen : ANSI green.\n"
				"\t\tyellow : ANSI yellow.\n"
				"\t\tcyan : ANSI cyan.\n"
				"\t\tmagenta : ANSI magenta.\n"
				"\t\tblack : ANSI black.\n"
				"\t\twhite : ANSI white.\n"
			"\tTo toggle BOLD use:\n"
			"\t\t1 : Bold Enabled\n"
			"\t\t0 : old Disabled\n");

	}

	/* store return code */
	/* rtCode = child_status;*/

	
}

void handle_EXIT(char **cmdTokens, int numTokens) {
	int i;
	/* TODO: remember to free cmd: from readline */

	/* free resource */
	free(pwd);
	/*TODO: free all elements in pathTokens
			do research on whether freeing the ** will suffice
	*/
	for(i=0; i<numTokens; i++) {
		free(cmdTokens[i]);
	}
	free(cmdTokens);

	for(i=0; i<numPathTokens; i++) {
		free(pathTokens[i]);
	}
	free(pathTokens);
	
	if(oldpwd!=NULL) {free(oldpwd);}

	/*free jobs w/names */
	freeAllJobs(jobPool);
	free(jobPool);

	if(cmd_readline!=NULL) {
		free(cmd_readline);
	}

	exit(EXIT_SUCCESS);

}

void handle_JOBS(JobPool *jobPool) {
	listJobs(jobPool);
}

void handle_FG(char **cmdTokens, int numTokens) {
	int child_status;
	pid_t wpid;
	if(numTokens==2) {
		Job *job = getJob(cmdTokens[1], jobPool, 0,0);
		if(job!=NULL) {
			printf("%s\n", job->name);
			// update current executing fg job
			currentFGJob = realloc(currentFGJob, strlen(job->name)+1);
			strcpy(currentFGJob, job->name);

			kill(job->pid, SIGCONT);
			job->running = 1;
			job->stopped = 0;

			// add only when there is not such id in fg list 
			if(!doesfg_pidExist(job->pid)) {
				addfg_pid(job->pid);
// #ifdef DEBUG
// printf("added %d to fg_pids\n", job->pid);
// #endif
			}
			wpid = waitpid(job->pid, &child_status, WUNTRACED);
			//TODO: remove it from the job list, and fg pid if done
			if(wpid == 0) {
				// still running
			} else if(wpid == job->pid) {
				// job done
				if(job->bg == 1) {
					//kill parent process
					kill(getpgid(job->pid),SIGKILL);
				}
				rmJob(NULL, jobPool, job->jid, 0);
				rmfg_pid(job->pid);
				//printf("[%d] %d Exited\n", job->jid, job->pid);
			}
			// _exit(EXIT_SUCCESS);
		} else {
			fprintf(stderr, "fg: %s: no such job\n", cmdTokens[1]);
		}
	} else {
		fprintf(stderr, "Usage: fg PID|JID\n");
	}
}

void handle_BG(char **cmdTokens, int numTokens) {
	int child_status;
	pid_t wpid;
	if(numTokens==2) {
		/*TODO: get job by pid/jid */
		Job *job = getJob(cmdTokens[1], jobPool,0,0);
		if(job!=NULL) {
			kill(job->pid, SIGCONT);
			job->running = 1;
			job->stopped = 0;

			wpid = waitpid(job->pid, &child_status, WNOHANG|WUNTRACED);
			//TODO: handle child's stoppage/termination
			if(wpid == 0) {
				// still running
			} else if(wpid == job->pid) {
				// job done
				// TODO: print msg 
				printf("[%d] %d Exited\n", job->jid, job->pid);
				rmJob(NULL, jobPool,0, job->pid);

			}
			// _exit(EXIT_SUCCESS);
		} else {
			fprintf(stderr, "fg: %s: no such job\n", cmdTokens[1]);
		}
		
	} else {
		fprintf(stderr, "Usage: fg PID|JID\n");
	}
}

/**
 * This builtin command should send the specified signal to the process specified by PID. 
 * If no signal is specified you must send SIGTERM as default. 
 * The signal is denoted by its number ranging from 1 to 31 (inclusive).
 * Usage: kill [signal] PID|JID
 */
void handle_KILL(char **cmdTokens, int numTokens) {
	Job *job;
	if(numTokens == 3) {
		job = getJob(cmdTokens[2], jobPool,0,0);
		
		if(job !=NULL) {
			if(kill(job->pid, atoi(cmdTokens[1]))==0) {

				printf("[%d] %d stopped by signal %s\n", job->jid, job->pid, cmdTokens[1]); 
				job->running = 0;
				job->stopped = 1;
			} else {
				// err in kill
				perror("kill");
			}
		} else {
			fprintf(stderr, "kill: %s: no such job\n", cmdTokens[2]);
		}
	} else if(numTokens == 2) {
		job = getJob(cmdTokens[1], jobPool,0,0);
		
		// send SIGTERM
		if(job !=NULL) {
			if(kill(job->pid, SIGTERM)==0) { 
				printf("[%d] %d stopped by signal %d\n", job->jid, job->pid, SIGTERM);
				job->running = 0;
				job->stopped = 1;
			} else {
				// err in kill
				perror("kill");
			}

		} else {
			fprintf(stderr, "kill: %s: no such job\n", cmdTokens[1]);
		}
	} else {
		fprintf(stderr, "Usage: kill [signal] PID|JID\n");
	}
}

void handle_DISOWN(char **cmdTokens, int numTokens) {
	if(numTokens==1) {
		/* remove all jobs from the list */
		freeAllJobs(jobPool);
	} else if (numTokens == 2) {
		/* remove specified job */
		if(rmJob(cmdTokens[1], jobPool, 0, 0)!=1) {
			// print error msg if arg isnt correct 
			fprintf(stderr, "disown: %s: no such job\n", cmdTokens[1]);
		}
	} else {
		/* incorrect cmd */
		fprintf(stderr, "Usage: disown [PID|JID]\n");
	}
}

/**
 * Tokenizes the cmd and store the tokens to cmdTokens
 * Returns number of pipes
 * @param cmd: cmd to be tokenized
 * @param cmdTokens: buffer for tokens
 * @param numTokens: number of tokens
 * @param redirect: flag to indicate redirection
 */
int tokenizeCMD(char *cmd, char **cmdTokens, int *numTokens, int *redirect, int *runInBg) {
    int numPipes = 0;
	int sq_flag = 1;
	int dq_flag = 2;

	/* no redirection yet */
	*redirect = 0;
	
	int i = 0; 	// cmd char index counter
	int k = -1;	// pointer index counter 
	/* Repeatedly read until the end of the cmd */
	while(cmd[i]!='\0') {

		if(cmd[i] == '&') {
			*runInBg = 1;
			break;
	 	}

		int j = 0;	// reset token char index counter 
		
		/* handle each token */
		while(1) {
			if(cmd[i] == ' ') {
				if(cmd[i+1] == '2' && cmd[i+2] == '>') {	
					// if the next char is 2 and the char after 2 is >
					// then it is a redirection token
					k++;	// IMPORTANT: now we will have two tokens produced in here
					cmdTokens[k] = malloc(sizeof(char));
					cmdTokens[k] = "2>";	
					*redirect = 1;
					i+=3;
					break;	// next token 
				} else if(sq_flag==-1 || dq_flag==-2) {
					cmdTokens[k][j++] = cmd[i];
				}
				// ignore space 
				i++;
			} else {
				//cmdTokens[k] = malloc(sizeof(char)*strlen(cmd));
	 			/* if pipe or other redirection operators are right after char, 
	 			   the the substr before the red. op. is one token, the red. op. is the other */
	 			if(cmd[i] == '&') {
	 				*runInBg = 1;
	 				break;
	 			} else if(cmd[i] == '|') {
	 				k++;
	 				//FIXME: mem leak 
	 				cmdTokens[k] = malloc(sizeof(char)*2);
	 				cmdTokens[k][0] = cmd[i];	// a redirection token
					cmdTokens[k][1] = '\0';
	 				numPipes++;
	 				i++;
	 				break;	// next token 
	 			} else if (cmd[i] == '>' || cmd[i] == '<') {
	 				k++;
	 				//FIXME: mem leak
	 				cmdTokens[k] = malloc(sizeof(char)*2);
	 				cmdTokens[k][0] = cmd[i];	// a redirection token
					cmdTokens[k][1] = '\0';
	 				*redirect = 1;
	 				i++;
	 				break;	// next token 
	 			} else if(cmd[i] != '\0') {
	 				if(j==0) {
	 					/* allocate enough of memory(max: the length of the cmd) for each token */
	 					k++;
	 					//FIXME: mem leak 
						cmdTokens[k] = malloc(sizeof(char)*strlen(cmd));
	 				}
					int flag = 0;
					if(cmd[i]=='\\') {
						if(cmd[i+1]=='\"' || cmd[i+1]=='\'') {
							cmdTokens[k][j++] = cmd[i+1];
							i+=2;
							flag = 1;
						}
					}
					if(flag==0) {
		 				cmdTokens[k][j++] = cmd[i];
						if(cmd[i]=='\'') {
							sq_flag*=(-1);
						} else if(cmd[i]=='\"') {
							dq_flag*=(-1);
						}
		 				i++;
					}
					if((sq_flag==1 && dq_flag==2) && 
					(cmd[i]==' ' || cmd[i] =='\0' || cmd[i] =='|' 
					|| cmd[i] =='<' || cmd[i] =='>' || cmd[i] =='&')){
						cmdTokens[k][j] = '\0';
			// printf("cmdTokens[%d] = %s\n", k, cmdTokens[k]);
						break;
					}

	 			} else {
	 				break;
	 			}
			}	
		}

	}
	/* store the number of tokens */
	*numTokens = k+1;
	
	/* remember, exec function family needs the array of pointer to 
	   have NULL at the end */
	cmdTokens[*numTokens] = NULL;
// printf("cmdTokens = %s\n", cmdTokens[0]);
	/* remove quotes in each token */
	rmQuote(cmdTokens, *numTokens);

    return numPipes;
}

/** 
 * Removes quotes pair in each token 
 */
void rmQuote(char **cmdTokens, int numTokens) {
	int i, j, k;

	/* single quote flag, 0 means all single quotes can be cleared
	 otherwise they cannot */
	int sflag = 0; 
	/* double quote flag, 0 means all double quotes can be cleared
	 otherwise they cannot */
	int dflag = 0;

	/* num_sq: number of single quote
 	   num_dq: number of double quote */
	int num_sq, num_dq;
	num_sq = num_dq = 0;

	k = 0; // counter of cmd char index

	for(i = 0; i < numTokens; i++) {

		/* loop 1: check number of single and double quotes */
		for(j = 0; j < strlen(cmdTokens[i]); j++) {
			if((cmdTokens[i][j] == '\'') && (cmdTokens[i][j-1] != '\\')) {
				num_sq++;
			} else if (cmdTokens[i][j] == '\"' && (cmdTokens[i][j-1] != '\\')) {
				num_dq++;
			}			
		}

		if(num_sq!=0 || num_dq!=0){
			sflag = num_sq%2;
			dflag = num_dq%2;
	
			char temp[strlen(cmdTokens[i])+1];
	
			/* loop 2: remove pairs of quotation marks */
			for(j = 0; j < strlen(cmdTokens[i]); j++) {
				if(cmdTokens[i][j] == '\'' && (cmdTokens[i][j-1] != '\\')) {
					/* if there is only 1 quote left, check whether it should be in the arg
					   otherwise move on, decrement num_sq */
					if(num_sq==1) {
						if(sflag!=0) {
							temp[k] = cmdTokens[i][j];
							k++;
						} else {
							num_sq--;
						}
					} else {
						num_sq--;
					}
					
				} 
				/*remove the next occurence of " */
				else if(cmdTokens[i][j] == '\"'  && (cmdTokens[i][j-1] != '\\')) {
					/* if there is only 1 quote left, check whether it should be in the arg
					   otherwise move on, decrement num_dq */
					if(num_dq==1) {
						if(dflag!=0) {
							temp[k] = cmdTokens[i][j];
							k++;
						} else {
							num_dq--;
						}
					} else {
						num_dq--;
					}
				} else {
					// construct updated cmd
					temp[k] = cmdTokens[i][j];
					k++;
				}
				
			}

			temp[k] = '\0';	// null terminate temp
			strcpy(cmdTokens[i], temp);	// quote rm'ed

			k = 0; // reset k
			sflag = dflag = 0;
		}
	}

}
/**
 * Checks if the cmd is a buildin
 */
int isBuildin(char *cmd) {
	if(strcmp(cmd, HELP)==0 || strcmp(cmd, EXIT)==0
	|| strcmp(cmd, CD)==0 	|| strcmp(cmd, PWD)==0 
	|| strcmp(cmd, PRT)==0 	|| strcmp(cmd, CHPMT)==0 
	|| strcmp(cmd, CHCLR)==0 || strcmp(cmd, JOBS)==0 
	|| strcmp(cmd, FG)==0 	|| strcmp(cmd, BG)==0 
	|| strcmp(cmd, KILL)==0 || strcmp(cmd, DISOWN)==0
	|| strcmp(cmd, "echo")==0 || strcmp(cmd, "set") == 0) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * Forwards cmd to appropriate handler.
 *
 * @param cmdTokens: buffer for tokens
 * @param numTokens: number of tokens
 * @param redirect: flag to indicate redirection
 */
void handleCMD(char *fullCMD, char **cmdTokens, int numTokens, int redirect) {
	/* identify buildin cmd */
	char *cmd = cmdTokens[0];
    
    if(redirect==1) {
        /* redirect flag is up, go to redirection handler */
        handleRedirection_NOPIPE(fullCMD, cmdTokens, numTokens);
        
    } else {
        /* no redirection, do normal operation */
        if(isBuildin(cmd)==1) {
            /* handle buildin */
            handleBuildin(cmdTokens, numTokens);
        } else {
            /* handle non buildin */
            handleNonBuildin(cmdTokens, numTokens);
        }
    }
}

void handleBuildin(char **cmdTokens, int numTokens) {
	char *cmd = cmdTokens[0];
	if(strcmp(cmd, HELP)==0) {
		handle_HELP();

	} else if(strcmp(cmd, CD)==0) {
		handle_CD(cmdTokens, numTokens);

	} else if(strcmp(cmd, PWD)==0) {
		handle_PWD();

	} else if(strcmp(cmd, EXIT)==0) {
		handle_EXIT(cmdTokens, numTokens);

	} else if(strcmp(cmd, PRT)==0) {
		handle_PRT();

	} else if(strcmp(cmd, CHPMT)==0) {
		handle_CHPMT(cmdTokens, numTokens);

	} else if(strcmp(cmd, CHCLR)==0) {
		handle_CHCLR(cmdTokens, numTokens);

	} else if(strcmp(cmd, JOBS)==0) {
		handle_JOBS(jobPool);

	} else if(strcmp(cmd, FG)==0) {
		handle_FG(cmdTokens, numTokens);

	} else if(strcmp(cmd, BG)==0) {
		handle_BG(cmdTokens, numTokens);

	} else if(strcmp(cmd, KILL)==0) {
		handle_KILL(cmdTokens, numTokens);

	} else if(strcmp(cmd, DISOWN)==0) {
		handle_DISOWN(cmdTokens, numTokens);

	} else if(strcmp(cmd, "echo")==0) {
		handle_ECHO(cmdTokens, numTokens);
	} else if(strcmp(cmd, "set") ==0) {
		handle_SET(cmdTokens, numTokens);
	}
}

void updatePrompt() {
	updatepwd();

	char pwdToDisplay[strlen(pwd)];

	if(strcmp(pwd, getenv("HOME")) == 0) {
		strcpy(pwdToDisplay, "~");
	} else if(strstr(pwd, getenv("HOME"))!=NULL) {
		
		//strcpy(pwdToDisplay,pwd);

		strcpy(pwdToDisplay, "~");
		strcat(pwdToDisplay, pwd+strlen(getenv("HOME")));
	} else {
		strcpy(pwdToDisplay, pwd);
	}
	if(user==1 && machine==1) {
		
		/* prompt should be "sfish-user@machine:[[pwd]] >"*/
		/* clear the prompt */
		memset(prompt, '\0', sizeof(prompt));
		/* rebuild the prompt */
		strcpy(prompt, PROMPT);
		strcat(prompt,"-");
		strcat(prompt, userColor);
		strcat(prompt, user_str);
		strcat(prompt, "\x1B[0m");
		strcat(prompt, "@");
		strcat(prompt, machineColor);
		strcat(prompt, machine_str);
		strcat(prompt,"\x1B[0m");
		strcat(prompt,":[");
		strcat(prompt, pwdToDisplay);
		strcat(prompt, "]> ");

	} else if(user==1) {
		/* prompt should be "sfish-user:[[pwd]] >"*/
		/* clear the prompt */
		memset(prompt, '\0', sizeof(prompt));
		/* rebuild the prompt */
		strcpy(prompt, PROMPT);
		strcat(prompt,"-");
		strcat(prompt, userColor);
		strcat(prompt, user_str);
		strcat(prompt,"\x1B[0m");
		strcat(prompt,":[");
		strcat(prompt, pwdToDisplay);
		strcat(prompt, "]> "); /* make sure the rest of the contents do not change color */

	} else if(machine==1) {
		/* prompt should be "sfish-machine:[[pwd]] >"*/
		/* clear the prompt */
		memset(prompt, '\0', sizeof(prompt));
		/* rebuild the prompt */
		strcpy(prompt, PROMPT);
		strcat(prompt,"-");
		strcat(prompt, machineColor);
		strcat(prompt, machine_str);
		strcat(prompt,"\x1B[0m");
		strcat(prompt,":[");
		strcat(prompt, pwdToDisplay);
		strcat(prompt, "]> "); 

	} else {
		/* prompt should be "sfish:[[pwd]] >"*/
		/* clear the prompt */
		memset(prompt, '\0', sizeof(prompt));
		/* rebuild the prompt */
		strcpy(prompt, PROMPT);
		strcat(prompt,":[");
		strcat(prompt, pwdToDisplay);
		strcat(prompt, "]> "); 
	}
}

/** 
 * Returns escape sequence of the given color, 
 * NULL is returned if no such color exits
 * @param bold: bold attr for color
 * @param what: what==0 if it is called for user, 
 *				what==1 if it is called for machine
 */
void setcolor(char *bold, char *color, int what) {
	char colorVal[3];

	/* get color value */
	if(strcmp(color, "red")==0) {
		strcpy(colorVal,"31");
	} else if(strcmp(color, "blue")==0) {
		strcpy(colorVal,"34");
	} else if(strcmp(color, "green")==0) {
		strcpy(colorVal,"32");
	} else if(strcmp(color, "yellow")==0) {
		strcpy(colorVal,"33");
	} else if(strcmp(color, "cyan")==0) {
		strcpy(colorVal,"36");
	} else if(strcmp(color, "magenta")==0) {
		strcpy(colorVal,"35");
	} else if(strcmp(color, "black")==0) {
		strcpy(colorVal,"30");
	} else if(strcmp(color, "white")==0) {
		strcpy(colorVal,"37");
	} else {
		/* not such color */
		/*TODO: print error msg */
		return ;
	}

	/* update color */
	if(what==0) {
		/* user */
		memset(userColor, '\0', COLOR_LENGTH);
		/* add bold attr and color */
		strcpy(userColor, "\x1B[");
		strcat(userColor, bold);
		strcat(userColor, ";");
		strcat(userColor, colorVal);
		strcat(userColor, "m");
		
	} else {
		/*machine*/
		memset(machineColor, '\0', COLOR_LENGTH);
		/* add bold attr and color */
		strcpy(machineColor, "\x1B[");
		strcat(machineColor, bold);
		strcat(machineColor, ";");
		strcat(machineColor, colorVal);
		strcat(machineColor, "m");
	}

}


void toggleUser(int toggle) {
	if(toggle == 1) {
		user=1;
	} else if(toggle == 0) {
		user=0;
	}
}
void toggleMachine(int toggle) {
	if(toggle == 1) {
		machine=1;
	} else if(toggle == 0) {
		machine=0;
	}
}

void toggleUserPmt_B(int toggle) {
	if(toggle == 1) {
		userPmt_B=1;
	} else if(toggle == 0) {
		userPmt_B=0;
	}
}

void toggleMachinePmt_B(int toggle) {
	if(toggle == 1) {
		machinePmt_B=1;
	} else if(toggle == 0) {
		machinePmt_B=0;
	}
}


/* Ctrl-H */
int helpMenu(int a, int b) {
	printf("\n%s", HELP_MENU);
	printf("%s", HELP_MENU2);
	return 0;
}
/* Ctrl-P */
int printInfo(int a, int b) {
	printf("\n----Info----\n"
	"help\n"
	"prt\n" 
	"----CTRL----\n" 
	"cd\n"
	"chclr\n"
	"chpmt\n"
	"pwd\n"
	"exit\n"
	"----Job Control----\n"
	"bg\n"
	"fg\n"
	"disown\n"
	"jobs\n"
	"----number of Commands Run----\n%d\n",num_cmd_ran);
	//TODO: print process table 
	return 0;
}

/* Ctrl-B */
int storePID(int a, int b) {

	Job *job = jobPool->head;
	if(job!=NULL) {
		spid=job->pid;
		printf("%d saved\n", spid);
	} else {
		spid=-1;
		fprintf(stderr, "\nNo pid to be saved\n");
	}

	return 0;
}

/* Ctrl-G */ 
int getPID(int a, int b) {
	if(spid != -1) {
		Job *job = getJob(NULL, jobPool, 0, spid);
		if(job->bg == 1) {
			kill(spid,SIGTERM);
			printf("\n[%d] %d stopped by signal %d\n", job->jid, job->pid, SIGTERM);
			rmJob(NULL, jobPool, 0, spid);
		} else {
			kill(spid,SIGSTOP);
			job->running = 0;
			job->stopped = 1;
			printf("\n[%d] %d stopped by signal %d\n", job->jid, job->pid, SIGSTOP);
		}
	} else {
		fprintf(stderr, "\nSPID does not exist and has been set to -1\n");
	}
	return 0;
}

