#include "sfish.h"
#include "functions.h"
#include "part2.h"
#include "redirection.h"
#include "jobctrl.h"
#include "extra_credit1.h"

/**
 * Handles echo buildin
 */
void handle_ECHO(char **cmdTokens, int numTokens) {
	if(numTokens==2 && cmdTokens[1][0] == '$'){
		pid_t pid;
		pid = fork();
		if(pid==0) {
			char *env = getenv(cmdTokens[1]+1);
			if(env!=NULL) {
				printf("%s\n", env);
				_exit(EXIT_SUCCESS);
			} else {
				fprintf(stderr, "echo: \"%s\" not set\n", cmdTokens[1]);
				_exit(EXIT_SUCCESS);
			}
		} else {
			waitpid(pid, NULL, 0);
		}
	} else {
		fprintf(stderr, "Usage: echo $ENVINRONMENT_VARIABLE\n");
	}
}

int isValidName(char *varName) {
	/* valid name will require that all variable names start with a $ character, 
	and only have either alphanumerical names or a single “special” character (e.g., ? , @ , etc.) */
	
	if(strlen(varName+1) == 2) {
		// only single special character is allowed
		if((varName[1] >= 33 && varName[1] <=47) || (varName[1] >= 58 && varName[1] <=64) 
		|| (varName[1] >= 91 && varName[1] <=96) || (varName[1] >= 123 && varName[1] <=126)) {
			return 1;
		} else {
			return 0;
		}
	} else {
		// only alphanumerical name is allowed
		int i = 1;
		// int rtval;
		while(varName[i] != '\0') {
			if((varName[i] >= 48 && varName[i] <=57) || (varName[i] >= 65 && varName[i] <=91)
			|| (varName[i] >= 97 && varName[i] <=122)) {
				// good so far
			} else {
				return 0;
			}
		}
		return 1;
	}
}

/**
 * Updates VALUE in case the set command is used to update an environment variable
 * returns 1 to indicate an update, 0 otherwise
 */
int updateValue(char *old, char *new) {
	int rtval;
	if(new[0] =='$') {
		int i;
		/* check if VALUE indicates an update of env */
		for(i=0; i<strlen(old); i++) {
			if(old[i] != new[i+1]) {
				rtval = 0;
				break;
			}
		}
		if(new[i+1]==':') {
			/* confirm it is update */
			char *oldEnv;
			if((oldEnv = getenv(old))!=NULL) {
				char newEnv[strlen(oldEnv) + strlen(new+1+strlen(old)) + 1];
				strcpy(newEnv, oldEnv);
				strcat(newEnv, new+1+strlen(old));
				setenv(old, newEnv, 1);
				rtval = 1;
			} else {
				/* env var havent been set up yet, print error */
				fprintf(stderr, "echo: \"%s\" not set\n", old);
			}
		} else {
			rtval = 0;
		}
	} else {
		rtval = 0;
	}

	return rtval;

}

void handle_SET(char **cmdTokens, int numTokens) {
	if(numTokens == 4 && strcmp("=",cmdTokens[2])==0) {
		if(isValidName(cmdTokens[1])) {
			/* check if there's env to be updated 
			   if there is, update it (inside the function)
			   otherwise, set 
			 */
			if(!updateValue(cmdTokens[1], cmdTokens[3])){
				setenv(cmdTokens[1], cmdTokens[3], 1);
			}
		} else {
			fprintf(stderr, "Invalid VARIABLE_NAME\n"
			"Valid name will require that all variable names start with a $ character, \n"
			"and only have either alphanumerical names or a single “special” character\n");
		}
	} else {
		fprintf(stderr, "Usage: set VARIABLE_NAME = VALUE\n");
	}
}

void updatePrtVal() {
	char rtCode_str[4];
	sprintf(rtCode_str, "%d", rtCode);
	setenv("?", rtCode_str, 1);
}
