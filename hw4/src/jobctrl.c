#include "jobctrl.h"
#include "functions.h"
#include "sfish.h"
#include "part2.h"
#include "redirection.h"

/**
 * List all jobs running in the background, their name, PID, job number. 
 * just like bash with their status (running or suspended). 
 * It should also print the exit status code of background jobs that just ended.
 */
void listJobs(JobPool *jobPool) {
	Job *job = jobPool->head;
	while(job!=NULL && jobPool->jobCounter!=0) {
		printf("[%d]    ", job->jid);
		if(job->running==1)
			printf("Running    ");
		else  if(job->stopped==1) {
			printf("Stopped    ");
		}
		printf("%d    ", job->pid);
		printf("%s\n", job->name);
		job = job->next;
	}
}

void addJob(Job *job, JobPool *jobPool) {

	if(jobPool->jobCounter == 0) {
		job->prev = NULL;
		job->next = NULL;
		jobPool->head = job;
		jobPool->tail = job;
		job->jid = 1;
	} else {
		job->next = NULL;
		job->prev = jobPool->tail;
		// jid is the old tail's jid+1
		job->jid = (jobPool->tail->jid)+1;
		jobPool->tail->next = job;
		jobPool->tail = job;
	}

	jobPool->jobCounter++;
}

Job *createJob(char *fullCMD, JobPool *jobPool) {
	Job *newJob = malloc(sizeof(Job));
	newJob->name = malloc(strlen(fullCMD)+1);
	strcpy(newJob->name, fullCMD);
	newJob->stopped = 0;
	newJob->running = 1;
	addJob(newJob, jobPool);
	return newJob;
}

/**
 * get job by string representation of p/jid, or 
 * int representation of jid (j) and pid (p)
 */
Job *getJob(char *pjid, JobPool *jobPool, int j, int p) {
	//TODO:
	Job *job = NULL;
	int jid;
	int pid;
	if(j != 0) {
		jid = j;
		Job *cursor = jobPool->head;
		while(cursor!=NULL) {
			if(cursor->jid == jid) {
				job = cursor;
				break;
			} else {
				cursor = cursor->next;
			}
		}
	} else if(pjid!=NULL && pjid[0]=='%') {
		/* search by jid */
		jid = atoi(pjid+1);
		if(jid!=0) {
			Job *cursor = jobPool->head;
			while(cursor!=NULL) {
				if(cursor->jid == jid) {
					job = cursor;
					break;
				} else {
					cursor = cursor->next;
				}
			}
		}
	} else if(p != 0) {
		pid = p;
		Job *cursor = jobPool->head;
		while(cursor!=NULL) {
			if(cursor->pid == pid) {
				job = cursor;
				break;
			} else {
				cursor = cursor->next;
			}
		}
	} else if(pjid!=NULL) {
		/* search by pid */
		pid = atoi(pjid);
		if(pid!=0) {
			Job *cursor = jobPool->head;
			while(cursor!=NULL) {
				if(cursor->pid == pid) {
					job = cursor;
					break;
				} else {
					cursor = cursor->next;
				}
			}
		}
	}
	return job;
}

/**
 * removes job specified by p/jid, returns 1 if success, 0 otherwise.
 */
int rmJob(char *pjid, JobPool *jobPool, int j, int p) {
	int success = 0;
	Job *cursor = jobPool->head;
	int jid, pid;
	if(j != 0) {
		/* search by jid */
		jid = j;
		while(cursor!=NULL) {
			if(cursor->jid == jid) {
				//FIXME: cursor can be head, and prev is NULL 
				if(cursor->prev != NULL) {
					cursor->prev->next = cursor->next;
				} 
				if(cursor->next != NULL) {
					cursor->next->prev = cursor->prev;
				}

				jobPool->jobCounter--;
				success = 1;
				break;
			} else {
				cursor = cursor->next;
			}
		}
	} else if(pjid!=NULL && pjid[0]=='%') {
		/* search by jid */
		jid = atoi(pjid+1);
		if(jid!=0) {
			while(cursor!=NULL) {
				if(cursor->jid == jid) {
					//FIXME: cursor can be head, and prev is NULL 
					if(cursor->prev != NULL) {
						cursor->prev->next = cursor->next;
					} 
					if(cursor->next != NULL) {
						cursor->next->prev = cursor->prev;
					}

					jobPool->jobCounter--;
					success = 1;
					break;
				} else {
					cursor = cursor->next;
				}
			}
		}
	} else if(p != 0) {
		/* search by pid */
		pid = p;	
		while(cursor!=NULL) {
			if(cursor->pid == pid) {
				// FIXME
				if(cursor->prev != NULL) {
					cursor->prev->next = cursor->next;
				} 
				if(cursor->next != NULL) {
					cursor->next->prev = cursor->prev;
				}
				jobPool->jobCounter--;
				success = 1;
				break;
			} else {
				cursor = cursor->next;
			}
		}
	} else if(pjid != NULL){
		/* search by pid */
		pid = atoi(pjid);
		if(pid!=0) {
			
			while(cursor!=NULL) {
				if(cursor->pid == pid) {
					// FIXME
					if(cursor->prev != NULL) {
						cursor->prev->next = cursor->next;
					} 
					if(cursor->next != NULL) {
						cursor->next->prev = cursor->prev;
					}
					jobPool->jobCounter--;
					success = 1;
					break;
				} else {
					cursor = cursor->next;
				}
			}
		}
	}
	if(success) {
		//TODO: print info, job stopped 
		// printf("%s\n", "job removed");
	} else {
		// TODO: error in rming 

	}
	return success;
}

/**
 * Ctrl+C Kills foreground program(s) using SIGINT 
 */
void handler_SIGINT(int sig) {
	// get process group id of foreground programs
	int i;
	for(i=0; i<fg_pids_counter; i++){
		rmJob(NULL, jobPool, 0,fg_pids[i]);
		kill(fg_pids[i], SIGKILL);
		rmfg_pid(fg_pids[i]);
		putchar('\n');
	}
}

/**
 *    Ctrl+Z : Suspends foreground program(s) using SIGTSTP
 */
void handler_SIGTSTP(int sig) {
	// suspsense foreground processes
	int i;
	for(i=0; i<fg_pids_counter; i++){
		// set signal to stop pid's proc
		kill(fg_pids[i], SIGSTOP);
		// waitpid(-1, NULL, WUNTRACED);
		// add to job list if havent done so 
		// 		 print job number, Stopped \t and cmd 
		/* check if this fg proc is already in the job list
		   if no, create job, set status as stopped */
		if(getJob(NULL, jobPool, 0, fg_pids[i])==NULL) {
			//FIXME: get cmd name for the job 
			char *temp = currentFGJob;
			Job *fgJob = createJob(temp, jobPool);
			fgJob->pid = fg_pids[i];
			fgJob->running = 0;
			fgJob->stopped = 1;
			fgJob->bg = 0;	// this is a fg job
			printf("\n[%d] %d Stopped\n", fgJob->jid, fgJob->pid);
			
		} else {
			Job *fgJob = getJob(NULL, jobPool, 0, fg_pids[i]);
			fgJob->running = 0;
			fgJob->stopped = 1;
			//printf("%s\n", fgJob->name);
			printf("\n[%d] %d Stopped\n", fgJob->jid, fgJob->pid);
		}
		//putchar('\n');

	}
}

/**
 * Handler for fg/gb, to resume the job
 */
void handler_SIGCONT(int sig) {
	//TODO: the handler_bg/fg function should call kill to send CONT signal
	// 
}

/* SIGTOU is sent when a bg process tries to write to terminal*/
void handler_SIGTTOU(int sig) {
	// 
}
/* SIGTOU is sent when a bg process tries to read from terminal*/
void handler_SIGTTIN(int sig) {
	// set job to be stopped 
}

void hanlder_SIGCHLD(int sig) {
	pid_t pid;
	int child_status;
	if((pid = waitpid(-1, &child_status, WNOHANG))!=-1) {
		// printf("%d Done\n", pid);
		//TODO: remove bg job from job list 
	}
}
void installSignalHanlders() {

	signal(SIGINT,  handler_SIGINT);
	signal(SIGTSTP, handler_SIGTSTP);
	// signal(SIGCONT, handler_SIGCONT);
	// signal(SIGTTOU, handler_SIGTTOU);
	// signal(SIGTTIN, handler_SIGTTIN);
	signal(SIGCHLD, hanlder_SIGCHLD);
}

void handleCMD_fg(char **cmdTokens, int numTokens) {

}

/**
 * when cmd is signaled to run program in bg, 
 * i.e. when cmd is "cmd &" 
 * this function is used as the handler  
 */
void handleCMD_bg(char **cmdTokens, int numTokens) {

}

/* adds fg proc's pid to the list */
void addfg_pid(pid_t pid) {
	int i;
	for(i=0; i<MAX_FG_PID; i++){
		if(fg_pids[i]==0){
			fg_pids[i]=pid;
			fg_pids_counter++;
			break;
		}

	}
}

void rmfg_pid(pid_t pid) {
	int i;
	for(i=0; i<MAX_FG_PID; i++){
		if(fg_pids[i]==pid){
			fg_pids[i]=0;
			fg_pids_counter--;
			break;
		}

	}
}

/** checks if fg pid exist in the list of fg pid, 
 * return 1 if found, 0 otherwise 
 */
int doesfg_pidExist(int pid) {
	int i;
	int rt = 0;
	for(i = 0; i < fg_pids_counter; i++) {
		printf("fg_pids[%d]=%d\n",i,fg_pids[i]);
		if(fg_pids[i] == pid) {
			rt = 1;
			break;
		}
	}
	return rt;
}

void handle_bg_cmd(char *fullCMD, char **cmdTokens, int numTokens, int redirect, int numPipes) {
	/*
	cmd comes in as bg proc 
	-> create job 
	-> add to job pool
	*/

	Job *newbgJob = createJob(fullCMD, jobPool);
	newbgJob->bg = 1;
	pid_t pid;
	int child_status;

	pid = fork();
	if(pid==0) {
		// TODO:
		// handle cmd 
		if(numPipes > 0) {
            /* if there's pipe, handle each process in pipe separately */
            handleRedirection_PIPE(fullCMD, cmdTokens, numTokens, numPipes);
        } else {
            /* identify and handle cmd */
            handleCMD(fullCMD, cmdTokens, numTokens, redirect);
            //printf("%d\n", getpid());
        }
        _exit(EXIT_SUCCESS);

	} else {
		newbgJob->pid = pid;
		printf("[%d] %d %s\n", newbgJob->jid, newbgJob->pid, newbgJob->name);
		pid_t wpid = waitpid(pid, &child_status, WNOHANG|WUNTRACED);
		//TODO: upon pressing enter, finished proc should print status
		if(wpid == pid) {
			// child is finished 
			//TODO: convert pid to string
			// rmJob(pid, jobPool, 0, 0);
			#ifdef DEBUG
			// printf("removing finished child: %d\n", pid);
			#endif
		} else if (wpid == 0) {
			// still running
			#ifdef DEBUG
			// printf("child: %d is still running\n", pid);
			#endif
		} else if(wpid < -1) {
			// error 
			// perror();
		}
		// rtCode = child_status;
	}
}

void freeAllJobs(JobPool *jobPool) {
	Job *next = jobPool->head;
	Job *cursor;
	while((cursor=next)!=NULL) {
		free(cursor->name);
		next = cursor->next;
		free(cursor);
		jobPool->jobCounter--;
	}
}

void updateJobPool() {
	Job *next = jobPool->head;
	Job *cursor;
	while((cursor=next)!=NULL) {
		// int rtval = 
		kill(cursor->pid,0);
	// #ifdef DEBUG
	// 	printf("kill(%d,0)=%d\n", cursor->pid, rtval);
	// #endif	

	// 	if(errno == ESRCH) {
	// 		// remove if from the job pool
	// 		printf("errno == ESRCH\n");
	// 	} else {
	// 		printf("errno != ESRCH\n");
	// 	}
		next = cursor->next;
	
	}
}

void updateFG_PIDList() {
	int i;
	for(i = 0; i < fg_pids_counter; i++) {
		if(getJob(NULL, jobPool, 0, fg_pids[i])==NULL) {
			rmfg_pid(fg_pids[i]);
		}
	}
}
