#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "jobctrl.h"

void initShell();

/**
 * Handles "help" command
 */
void handle_HELP();
void handle_CD(char **cmdTokens, int numTokens);

/**
 * Handles the pwd buildin - prints the pwd
 */
void handle_PWD();

void updatepwd();

/**
 * Handles the prt buildin - prints the return code of the previous command
 */
void handle_PRT();

/*Usage: chpmt SETTING TOGGLE*/
void handle_CHPMT(char **cmdTokens, int numTokens);

/* Usage: chclr SETTING COLOR BOLD*/
void handle_CHCLR(char **cmdTokens, int numTokens);

void handle_EXIT(char **cmdTokens, int numTokens);
void handle_JOBS(JobPool *jobPool);
void handle_FG(char **cmdTokens, int numTokens);
void handle_BG(char **cmdTokens, int numTokens);
void handle_KILL(char **cmdTokens, int numTokens);
void handle_DISOWN(char **cmdTokens, int numTokens);

/**
 * Tokenizes the cmd and store the tokens to cmdTokens
 * Returns number of pipes
 * @param cmd: cmd to be tokenized
 * @param cmdTokens: buffer for tokens
 * @param numTokens: number of tokens
 * @param redirect: flag to indicate redirection
 */
int tokenizeCMD(char *cmd, char **cmdTokens, int *numTokens, int *redirect, int *runInBg);

/**
 * Removes quotes pair in each token
 */
void rmQuote(char **cmdTokens, int numTokens);
/**
 * Checks if the cmd is a buildin
 */
int isBuildin(char *cmd);
/**
 * Forwards cmd to appropriate handler.
 *
 * @param cmdTokens: buffer for tokens
 * @param numTokens: number of tokens
 * @param redirect: flag to indicate redirection
 */
void handleCMD(char *fullCMD, char **cmdTokens, int numTokens, int redirect);
void handleBuildin(char **cmdTokens, int numTokens);
void updatePrompt();
/** 
 * Returns escape sequence of the given color, 
 * NULL is returned if no such color exits
 * @param bold: bold attr for color
 * @param what: what==0 if it is called for user, 
 *				what==1 if it is called for machine
 */
void setcolor(char *bold, char *color, int what);
void toggleUser(int toggle);
void toggleMachine(int toggle);
void toggleUserPmt_B(int toggle);
void toggleMachinePmt_B(int toggle);

/* functions for key binding */
int helpMenu(int a, int b);
int printInfo(int a, int b);
int storePID(int a, int b);
int getPID(int a, int b);

// void handle_ECHO(char **cmdTokens, int numTokens);

// int isValidName(char *varName);
/**
 * Updates VALUE in case the set command is used to update an environment variable
 * returns 1 to indicate an update, 0 otherwise
 */
// int updateValue(char *o, char *n);
// void handle_SET(char **cmdTokens, int numTokens);
// void updatePrtVal();

#endif
