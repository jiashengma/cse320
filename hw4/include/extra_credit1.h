#ifndef EXTRA_CREDIT1
#define EXTRA_CREDIT1

void handle_ECHO(char **cmdTokens, int numTokens);

int isValidName(char *varName);
/**
 * Updates VALUE in case the set command is used to update an environment variable
 * returns 1 to indicate an update, 0 otherwise
 */
int updateValue(char *o, char *n);
void handle_SET(char **cmdTokens, int numTokens);
void updatePrtVal();

#endif