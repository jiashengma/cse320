#ifndef JOBCTRL_H
#define JOBCTRL_H

#include "sfish.h"


struct Job{
	int jid;
	int pid;
	int bg; // flag: 1 -> job is bg process, 0 -> fg process
	char *name;
	int stopped, running; // status flags 
	struct Job *prev;
	struct Job *next;
};
typedef struct Job Job;

struct JobPool {
	int jobCounter;
	Job *head;
	Job *tail;
};
typedef struct JobPool JobPool;

extern JobPool *jobPool;
/* a list of fg pids */
extern pid_t fg_pids[MAX_FG_PID];
extern int fg_pids_counter;

void listJobs(JobPool *jobPool);
void addJob(Job *job, JobPool *jobPool);
int rmJob(char *pjid, JobPool *jobPool, int j, int p);
int doesfg_pidExist(int pid);
Job *getJob(char *pjid, JobPool *jobPool, int j, pid_t p);
void handler_SIGINT(int sig);
void handler_SIGTSTP(int sig);
void handler_SIGTTOU(int sig);
/* SIGTOU is sent when a bg process tries to read from terminal*/
void handler_SIGTTIN(int sig);
void hanlder_SIGCHLD(int sig);
void installSignalHanlders();
void handleCMD_fg(char **cmdTokens, int numTokens);
void handleCMD_bg(char **cmdTokens, int numTokens);
void addfg_pid(pid_t pid);
void rmfg_pid(pid_t pid);
void handle_bg_cmd(char *fullCMD, char **cmdTokens, int numTokens, int redirect, int numPipes);
void freeAllJobs(JobPool *jobPool);
void updateJobPool();
void updateFG_PIDList();

#endif
