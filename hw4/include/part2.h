#ifndef PART2_H
#define PART2_H
#include "sfish.h"

extern char **pathTokens;
extern int numPathTokens; 

/** 
 * determines whether program indicated by path exists
 * if so, it returns 1, 0 otherwise
 */
char* findProgram(char *path);
pid_t handleNonBuildin(char **cmdTokens, int numTokens);
void tokenizePATH();

#endif
