#ifndef REDIRECTION_H
#define REDIRECTION_H

/**
 * @param prog: first cmd, the path of the program to be executed
 */
int handleRedirection_NOPIPE(char *fullCMD, char **cmdTokens, int numTokens);

int handleRedirection_PIPE(char *fullCMD, char **cmdTokens, int numTokens, int numPipes);

void closeAllPipes(int pipes[][2], int numPipes);

#endif
