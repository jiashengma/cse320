#ifndef SFISH_H
#define SFISH_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <readline/readline.h>
#include <readline/history.h>

#define HELP "help"
#define EXIT "exit"
#define CD "cd"
#define PWD "pwd"
#define PRT "prt"
#define CHPMT "chpmt"
#define CHCLR "chclr"
#define JOBS "jobs"
#define BG "bg"
#define FG "fg"
#define KILL "kill"
#define DISOWN "disown"

#define MAX_PATH 128
#define MAX_HOSTNAME 128
#define MAX_PROMPT 1024
#define COLOR_LENGTH 16
#define MAX_FG_PID 128
#define PROMPT "sfish"
#define HELP_MENU "help\n"\
			"exit\n"\
			"cd [dir]\n"\
			"pwd\n"\
			"prt\n"\
			"chpmt SETTING TOGGLE\n"\
			"chclr SETTING COLOR BOLD\n"\
			"jobs"

#define HELP_MENU2 "fg PID|JID\n"\
			"bg PID|JID\n"\
			"kill [signal] PID|JID\n"\
			"disown [PID|JID]\n"\
			"echo $ENVINRONMENT_VARIABLE\n"\
			"set VARIABLE_NAME = VALUE\n"

extern int rtCode;
extern char *cmd_readline;

/* prompt flags */
extern int user, machine;
extern char user_str[MAX_HOSTNAME], machine_str[MAX_HOSTNAME];
/* prompt bold flags */
extern int userPmt_B, machinePmt_B; 

extern char userColor[COLOR_LENGTH], machineColor[COLOR_LENGTH];

extern char *pwd;
extern char *oldpwd;
extern char prompt[MAX_PROMPT];

/* redirection, pipeflag, 1 yes, 0 no*/
extern int redirect;
/* flag to indicate whether cmd should run in bg */
extern int runInBg;

extern pid_t fg_pgid; 
extern pid_t bg_pgid; 

extern int num_cmd_ran;
extern pid_t spid;
extern char *currentFGJob;


/**
 * Determines whether the argument is balanced with its quote
 */
int isQuotedArgBalanced(char *cmd);
int findQuoteIndex(char *cmd, int quote);
void balanceQuote(char *cmd_readline, int target_quote);
#endif
