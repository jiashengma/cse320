# Lord of the Threads

## Author: Jia Sheng Ma
### ID: 109696764

Note: 
	* With the "nthreads" argument, the works are divided as follows:
		- If the number of file entry (call it n) is greater than nthreads, 
			then take the <b>int</b> quotient of n and nthreads (call it Q).
			Each of the 0th to (nthread-1)th threads will have Q entries to work on,
			the nth thread will have the rest entries (which is generally greater than Q)
		
		- If the number of file entry is equal to nthreads, then each thread will
			have the same number of entires to work on.
	
	* Parts 1 and 2 do not have the "reduce" threads because we are performing
		the reduce operation only once.

	* Made sloppy designs.. 
		Thought all parts were independent of each other 
		(had to write many same functionalites over for many parts)
