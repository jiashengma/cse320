#ifndef HEADER_H
#define HEADER_H

#include <semaphore.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include "lott.h"

#define IP_LEN 16
#define NUM_ENTRY 100000
#define NUM_COUNTRY_CODE 10
#define COUNTRY_CODE_LEN 3


// #define NUM_YEAR 48
// #define START_YEAR 1969
#define NUM_YEAR 138
#define START_YEAR 1901
#define OUTFILE "mapred.tmp"
#define NAME_LEN 32
#define NUM_FD 2
#define USLEEP_TIME 500
#define BUFFER_SIZE 20

/*
	A/B: Max/Min Average duration;
	map(): Average duration for each website
	reduce(): Max/Min average duration of all of the websites

	C/D: Max/Min Average # of users per year
	map(): Find the average user count per year 
	reduce(): Max/Min average between all sites

	E: Country with most users
	map(): Count country codes to find maximum occurence for a website 
	reduce(): Find country with the most users
*/

/**
 * Stores info for each file (website)
 */
struct WebInfo {
	char *filename;
	char ct_w_maxUser[COUNTRY_CODE_LEN];
	char countryCode[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN];
	int countryCodeCounter[NUM_COUNTRY_CODE];

	double aveDuration;	/* average duration in minute */
	double aveUser;		/* Average # of users per year */
	int num_maxUser; 	/* number of users for the country that has maximum user */

	/* user counter starts int year 1969 ends in 2016 */
	int user_yr_counter[NUM_YEAR];	/* 2016-1970+1 = 46+1 years(internet started in 1969) */

	
	/* number of WebInfo/website entry
	   this is only used for reduce() */
	int num_webInfo_entry;

	/* number of entry per portion for nthreads */
	int portion;

};
typedef struct WebInfo WebInfo;

struct global_buf {
	/* number of item in the buffer */
	int n;
	/* head index of buffer */
	int index_head;
	/* tail index of buffer */
	int index_tail;
	/* buffer to hold data */
	WebInfo *webInfo_result_buf;

	sem_t mutex; 
	sem_t empty_slot;
	sem_t filled_slot;
};
typedef struct global_buf global_buf;

#endif