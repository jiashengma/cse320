#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <sys/types.h>
#include <dirent.h>
#include "header.h"

WebInfo* findMaxAveDuration(WebInfo *allWebInfo);

/**
 * Finds minimum duration among all sites
 * Returns the min duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMinAveDuration(WebInfo *allWebInfo);

void initUserYearArray(WebInfo *webInfo);

void processQuery(WebInfo *reducedWebInfo);

/**
 * Frees memory allocated to filenames 
 */
void freeFilenames(WebInfo *all_webInfo, int num_filename);

/**
 * Stores time stamp in epoch to web info struct as year 
 */
void handleTimestamp(WebInfo *webInfo, long timeStamp);


void calculateAveUser(WebInfo *webInfo);
/**
 * Finds maximum average user per year among all sites
 * Returns the max duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMaxAveUser(WebInfo *allWebInfo);
/**
 * Finds minimum average user per year among all sites
 * Returns the min duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMinAveUser(WebInfo *allWebInfo);


void initCountryCodeArray(WebInfo *webInfo);

void initCountryCodeArray(WebInfo *webInfo);

void handleContryCode(WebInfo *webInfo, char countryCode[]);


void findCountryWMaxUser(WebInfo *webInfo);


/**
 * Finds the country with most users among all sites
 * R stands for "Reduce"
 */
WebInfo* findCountryWMaxUserR(WebInfo *allWebInfo);


void updateTempResult(WebInfo *tempResult, WebInfo *result, char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc);

void composeDataToWrite(char *buf, WebInfo *temp);

void identifyAndUpdateResult(char *result, WebInfo *tempResult, WebInfo *result_aggregate, 
							char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc);

void initCC_UC(char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc);

void getMaxUserAndItsCountry(char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc, char *country, int *num_maxUser);

#endif