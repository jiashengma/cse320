#include "lott.h"
#include "functions.h"
#include "header.h"

static void* map(void*);
static void* reduce(void*);

int part2(size_t nthreads) {

    int i, j, entryCounter;    /* counters */
    
    DIR *dir;               /* directory where test files are */
    struct dirent *entry;   /* directory entries */
    
    char dataDir[strlen(DATA_DIR)*sizeof(char) + 2];
    
    WebInfo *all_webInfo;  /* list of info structures */
    WebInfo *result;

    int numEntryPerThread;

    strcpy(dataDir, DATA_DIR);
    // strcat(dataDir, "/");

    if ((dir = opendir(dataDir)) == NULL){
        perror("opendir()");
    } else {
        entryCounter = 0;
        /* Run a first pass to get the number of entries in the directory */
        while ((entry = readdir(dir)) != NULL){
            if(entry->d_type==DT_REG){
                entryCounter++;            
            }
        }
        
        /* list of info structures */
        all_webInfo = (WebInfo*)malloc(sizeof(WebInfo) * entryCounter);  
        

        /**********************************************************
         * divide up the work according total number of entry and *
         * number of thread given                                 *
         * 
         * The first n - 1 thread are equal in entry, the last    *
         * thread may not, it could have equal or less or more 
         * entry      
         *
         * If nthreads is greater than number of entry,           
         * then 1 thread
        ***********************************************************/
        if(entryCounter < nthreads) {
            numEntryPerThread = 1;
        } else {
            numEntryPerThread = entryCounter/nthreads;
        }
        /* list of thread ids */
        pthread_t tids[nthreads];

        /* rewind for spawning threads */
        rewinddir(dir);

        /* read and store file names */
        i = 0;
        while ((entry = readdir(dir)) != NULL){
            /* only look at regular files, so that . and .. are excluded */
            if(entry->d_type==DT_REG){
                /* store file name */
                (all_webInfo+i)->filename = malloc((strlen(DATA_DIR) + 1 + strlen(entry->d_name))*sizeof(char) + 1);
                strcpy((all_webInfo+i)->filename, DATA_DIR);
                strcat((all_webInfo+i)->filename, "/");
                strcat((all_webInfo+i)->filename, entry->d_name);
                i++;

            }
        }

        //TODO 
        j = 0;
        int portion = 1;
        /* spawn threads */
        for(i = 0; i < entryCounter; i+=portion){
            if(j < nthreads){
                char name[NAME_LEN];   /* thread name */
    
                // TODO: pass start ptr to each portion of all_webinfo
                // int portion;
                if(j == nthreads-1) {
                    /* the last portion could have equal or less or more entries */
                    portion = entryCounter - j*numEntryPerThread;
                } else {
                    portion = numEntryPerThread;
                }
                (all_webInfo+i)->portion = portion;
                pthread_create(&tids[j], NULL, map, (void*)(all_webInfo+i));
                
                /* name thread */
                sprintf(name, "map%d", j);
                pthread_setname_np(tids[j], name);
                j++;
            }
        }
        
        /* join threads */
        for(i = 0; i < j; i++) {
            pthread_join(tids[i], NULL);
        }

        /* store number of entry, for use of reduce() */
        all_webInfo->num_webInfo_entry = entryCounter;  
        /* reduce */
        result = (WebInfo*)reduce(all_webInfo);

        /* print result */
        printf("Part: %s\nQuery: %s\n", PART_STRINGS[current_part], QUERY_STRINGS[current_query]);
        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            printf("Result: %.5g, ", result->aveDuration);
            printf("%s\n", result->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0) {
            printf("Result: %.5g, ", result->aveUser);
            printf("%s\n", result->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            //TODO: check the right output 
            printf("Result: %d, %s\n",result->num_maxUser,  result->ct_w_maxUser);
        }

        /* clean up */
        freeFilenames(all_webInfo, i);
        free(all_webInfo);

        closedir(dir);
    }
    return 0;
}

static void* map(void* v){
    /* pointer */
    WebInfo *webInfo = (WebInfo*)v;
    WebInfo *temp;
    /* counter */
    int i, j;

    /* number of file to process in a thread */
    int n = webInfo->portion;
    
    /* do work/map */
    for(i = 0; i < n; i++){
        /* data format: UNIX_TIMESTAMP, IP, DURATION, COUNTRY */
        long timeStamp;
        char ip[IP_LEN];
        double duration;
        char countryCode[COUNTRY_CODE_LEN]; 
        double totalDuration = 0.0;
        FILE *f;

        /* update webInfo struct pointer */
        temp = webInfo + i;
        f = fopen(temp->filename, "r");
    
        /* init arrays */
        initUserYearArray(temp);
        initCountryCodeArray(temp);
    
        j = 0;
        /* read and store website info */
        while(fscanf(f, "%ld,%[^,],%lf,%[^\n]\n", &timeStamp, ip, &duration, countryCode) != EOF) {
            totalDuration+=duration;
    
            // convert time stamp to year to calculate ave usr/year
            handleTimestamp(temp, timeStamp);
    
            // tally country code 
            handleContryCode(temp, countryCode);
            j++;
        }
        fclose(f);
    
        /* calculate and store average duration */
        temp->aveDuration = totalDuration/j;
        /* calculate and store average user per year */
        calculateAveUser(temp);
        /* find country with the most users */
        findCountryWMaxUser(temp);
    }

    return v;
}


/**
 * Reduces the list of web info struct to a result struct
 * store results in the struct passed in (maybe reallocate mem)
 */
static void* reduce(void* v){
    WebInfo *allWebInfo = (WebInfo*)v;

    if(strcmp(QUERY_STRINGS[current_query], "A")==0) {
        /* find max average duration of all of the websites */
        allWebInfo = findMaxAveDuration(allWebInfo);
        //TODO: remove data/ in filename
    } else if(strcmp(QUERY_STRINGS[current_query], "B")==0) {
        /* find min average duration of all of the websites */
        allWebInfo = findMinAveDuration(allWebInfo);

    } else if(strcmp(QUERY_STRINGS[current_query], "C")==0) {
        /* Max Average # of users per year */
        allWebInfo = findMaxAveUser(allWebInfo);

    } else if(strcmp(QUERY_STRINGS[current_query], "D")==0) {
        /* Min Average # of users per year */
        allWebInfo = findMinAveUser(allWebInfo);

    } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
        /* country with most users */
        allWebInfo = findCountryWMaxUserR(allWebInfo);
    } else {
        /* NO SUCH QUERY */
    }

    return allWebInfo;
}
