#include "lott.h"
#include "functions.h"
#include "header.h"


static void* map(void*);
static void* reduce(void*);

int sockets[2]; /* 0-read(reduce), 1-write(map) */
struct pollfd pollfds[NUM_FD];

/* used exclusively for E */
char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN];
int uc[NUM_COUNTRY_CODE];

/* flag to indicate map threads are joined */
short isMapJoined;


/*
    Read one line and fill the buffer
*/
int Read(int fd, char *buf) {
    int rtval;
    int i = 0;

    //TODO: may need mutex
 
    while((rtval = read(sockets[0], buf+i, 1)) > 0){
        i++;
        if(buf[i-1] == '\n'){
            // null terminate instead, for parsing
            buf[i-1] = '\0';
            break;
        }
    }

    return rtval;
}

int part5(size_t nthreads){

    int i, j, entryCounter;    /* counters */
    int numEntryPerThread;
    DIR *dir;               /* directory where test files are */
    struct dirent *entry;   /* directory entries */
    
    char dataDir[strlen(DATA_DIR)*sizeof(char) + 2];
    
    WebInfo *all_webInfo;  /* list of info structures */
    WebInfo *result_aggregate = malloc(sizeof(WebInfo));  /* for storing temp result */

    /* set the result's filename to NULL for reduce's comparison use */
    // TODO: free filename 
    result_aggregate->filename = NULL;  
    
    strcpy(dataDir, DATA_DIR);

    if ((dir = opendir(dataDir)) == NULL){
        perror("opendir()");
    } else {
        entryCounter = 0;
        /* Run a first pass to get the number of entries in the directory */
        while ((entry = readdir(dir)) != NULL){
            if(entry->d_type==DT_REG){
                entryCounter++;            
            }
        }
        
        /* list of info structures */
        all_webInfo = (WebInfo*)malloc(sizeof(WebInfo) * entryCounter);  
        initCC_UC(cc, uc);

        /**********************************************************
         * divide up the work according total number of entry and *
         * number of thread given                                 *
         * 
         * The first n - 1 thread are equal in entry, the last    *
         * thread may not, it could have equal or less or more 
         * entry      
         *
         * If nthreads is greater than number of entry,           
         * then 1 thread
        ***********************************************************/
        //TODO: when nthreads == 0, throw error  
        if(entryCounter < nthreads) {
            numEntryPerThread = 1;
        } else {
            numEntryPerThread = entryCounter/nthreads;
        } /* end partitioning */


        /* list of thread ids */
        pthread_t tids[nthreads];
        pthread_t tid_reduce;

        /* rewind for spawning threads */
        rewinddir(dir);

        /* read and store file names */
        i = 0;
        while ((entry = readdir(dir)) != NULL){
            /* only look at regular files, so that . and .. are excluded */
            if(entry->d_type==DT_REG){
                /* store file name */
                (all_webInfo+i)->filename = malloc((strlen(DATA_DIR) + 1 + strlen(entry->d_name))*sizeof(char) + 1);
                strcpy((all_webInfo+i)->filename, DATA_DIR);
                strcat((all_webInfo+i)->filename, "/");
                strcat((all_webInfo+i)->filename, entry->d_name);
                i++;

            }
        }
        
        /*********************************************
                    create socket pair
        *********************************************/
        socketpair(AF_UNIX, SOCK_STREAM, 0, sockets);

        /*********************************************
                        map threads 
        *********************************************/
        isMapJoined = 0;
        j = 0;
        int portion = 1;
        for(i = 0; i < entryCounter; i+=portion){
            if(j < nthreads){
                char name[NAME_LEN];   /* thread name */
    
                if(j == nthreads-1) {
                    /* the last portion could have equal or less or more entries */
                    portion = entryCounter - j*numEntryPerThread;
                } else {
                    portion = numEntryPerThread;
                }
                (all_webInfo+i)->portion = portion;
                pthread_create(&tids[j], NULL, map, (void*)(all_webInfo+i));
                
                /* name thread */
                sprintf(name, "map%d", j);
                pthread_setname_np(tids[j], name);
                j++;
            }
        }
        /*********************************************
                        reduce thread
        *********************************************/        
         /* create reader thread */
        pthread_create(&tid_reduce, NULL, reduce, result_aggregate/* buffer */);
        /* name thread */
        pthread_setname_np(tid_reduce, "reduce");
        
        /*********************************************
                        conclude
        *********************************************/
        for(i = 0; i < j; i++) {
            pthread_join(tids[i], NULL);
        }
        isMapJoined = 1;
        close(sockets[1]);

        // /* cancel reader thread */
        //pthread_cancel(tid_reduce);
        pthread_join(tid_reduce, NULL);

        /* print result */
        printf("Part: %s\nQuery: %s\n", PART_STRINGS[current_part], QUERY_STRINGS[current_query]);
        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            printf("Result: %.5g, ", result_aggregate->aveDuration);
            printf("%s\n", result_aggregate->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0) {
            printf("Result: %.5g, ", result_aggregate->aveUser);
            printf("%s\n", result_aggregate->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            getMaxUserAndItsCountry(cc, uc, result_aggregate->ct_w_maxUser, &(result_aggregate->num_maxUser));
            printf("Result: %d, %s\n",result_aggregate->num_maxUser,  result_aggregate->ct_w_maxUser);
        }
        /*********************************************
                    clean up 
        *********************************************/
        close(sockets[0]);
        // close(sockets[1]);
        freeFilenames(all_webInfo, i);
        free(all_webInfo);
        free(result_aggregate->filename);
        free(result_aggregate);

        closedir(dir);
    }
    return 0;
}

static void* map(void* v){
    /* pointer */
    WebInfo *webInfo = (WebInfo*)v;
    WebInfo *temp;
    /* counter */
    int i, j;

    /* number of file to process in a thread */
    int n = webInfo->portion;
    
    /* do work/map */
    for(i = 0; i < n; i++){
        /* data format: UNIX_TIMESTAMP, IP, DURATION, COUNTRY */
        long timeStamp;
        char ip[IP_LEN];
        double duration;
        char countryCode[COUNTRY_CODE_LEN]; 
        double totalDuration = 0.0;
        FILE *f;

        /* update webInfo struct pointer */
        temp = webInfo + i;
        f = fopen(temp->filename, "r");
    
        /* init arrays */
        initUserYearArray(temp);
        initCountryCodeArray(temp);
    
        j = 0;
        /* read and store website info */
        while(fscanf(f, "%ld,%[^,],%lf,%[^\n]\n", &timeStamp, ip, &duration, countryCode) != EOF) {
            totalDuration+=duration;
    
            // convert time stamp to year to calculate ave usr/year
            handleTimestamp(temp, timeStamp);
    
            // tally country code 
            handleContryCode(temp, countryCode);
            j++;
        }
        fclose(f);
    
        /* string buffer for data to be written out */
        char buf[256];
        /* calculate and store average duration */
        temp->aveDuration = totalDuration/j;
        /* calculate and store average user per year */
        calculateAveUser(temp);
        /* find country with the most users */
        findCountryWMaxUser(temp);

        /* format data to write to file */
        composeDataToWrite(buf, temp);
        /*********************************************
                write data to unix socket
        *********************************************/
        pollfds[1].fd = sockets[1];    /* setup unix socket to write to */
        write(sockets[1], buf, strlen(buf));

        //TODO: close fds after finishing?

    }

    return v;
}


/**
 * Reduces the list of web info struct to a result struct
 * store results in the struct passed in (maybe reallocate mem)
 */
static void* reduce(void* v){

    char buf[256], result[256], filename[256];  /* buffer for reading data */
    int i;  // for reading bytes into buffer 
    WebInfo *result_aggregate = (WebInfo*)v;    /* final result (aggragate)*/
    int rtval;

    /* setup unix socket to read from */
    pollfds[0].fd = sockets[0];   

    while(1){
        pollfds[0].events = POLLIN | POLLRDHUP;     /* socket0 interested in read event*/
        pollfds[1].events = POLLOUT;    /* socket1 interested in write event */

        /* wait for events */
        poll(pollfds, NUM_FD, -1);
        
        i = 0;
        (void )i;
        /* read data from unix socket */
        if (pollfds[0].revents & POLLIN) {
            while((rtval=Read(sockets[0], buf)) > 0) {
                // printf("* %s *\n", buf);
                sscanf(buf, "%[^,],%s\n", result, filename);
                WebInfo *tempResult = malloc(sizeof(WebInfo));
                tempResult->filename = filename;

                /* identify and update result */
                identifyAndUpdateResult(result, tempResult, result_aggregate, cc, uc);

                free(tempResult);
            }
        }

        if(pollfds[0].revents & POLLRDHUP || pollfds[0].revents & POLLHUP ) {
            // puts("socket1 closed");
            // TODO:read till eof
            break;
        }
    }
    // puts("join reduce");

    return result_aggregate;
}
