#include "lott.h"
#include "functions.h"
#include "header.h"

static void* map(void*);
static void* reduce(void*);

int part1(){
    /*      procedure:
        1. read directory
        2. for each file in the directory
            - spawn thread that calls map(): pthread_create()
            - pthread_setname_np(pthread_t thread, const char *name);

        3. for each thread
            call pthread_join()
        4. reduce
            pthread_setname_np(pthread_t thread, "reduce");
     */

    int i, entryCounter;/* counters */
    DIR *dir;               /* directory where test files are */
    struct dirent *entry;   /* directory entries */
    WebInfo *result;
    char dataDir[strlen(DATA_DIR)*sizeof(char) + 2];
    WebInfo *all_webInfo;  /* list of info structures */

    strcpy(dataDir, DATA_DIR);
    // strcat(dataDir, "/");

    if ((dir = opendir(dataDir)) == NULL){
        perror("opendir()");
    } else {
        entryCounter = 0;
        /* Run a first pass to get the number of entries in the directory */
        while ((entry = readdir(dir)) != NULL){
            if(entry->d_type==DT_REG){
                entryCounter++;            
            }
        }

        all_webInfo = (WebInfo*)malloc(sizeof(WebInfo) * entryCounter);  /* list of info structures */
        pthread_t tids[entryCounter];   /* list of thread ids */

        /* rewind for spawning threads */
        rewinddir(dir);
        /* spawn threads */
        i = 0;
        while ((entry = readdir(dir)) != NULL){
            /* only look at regular files, so that . and .. are excluded*/
            if(entry->d_type==DT_REG){  
                char name[NAME_LEN];   /* thread name */

                /* store file name */
                (all_webInfo+i)->filename = malloc((strlen(DATA_DIR) + 1 + strlen(entry->d_name))*sizeof(char) + 1);
                strcpy((all_webInfo+i)->filename, DATA_DIR);
                strcat((all_webInfo+i)->filename, "/");
                strcat((all_webInfo+i)->filename, entry->d_name);

                /* create thread */
                pthread_create(&tids[i], NULL, map, (void*)(all_webInfo+i));
                
                /* name thread */
                sprintf(name, "map%d", i);
                pthread_setname_np(tids[i], name);
                i++;
            }
        }
        
        /* join threads */
        for(i = 0; i < entryCounter; i++) {
            pthread_join(tids[i], NULL);
        }

        /* reduce */
        all_webInfo->num_webInfo_entry = entryCounter;  /* store number of entry, for use of reduce() */
        result = (WebInfo*)reduce(all_webInfo);

        /* print result */
        printf("Part: %s\nQuery: %s\n", PART_STRINGS[current_part], QUERY_STRINGS[current_query]);
        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            printf("Result: %.5g, ", result->aveDuration);
            printf("%s\n", result->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0) {
            printf("Result: %.5g, ", result->aveUser);
            printf("%s\n", result->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            //TODO: check the right output 
            printf("Result: %d, %s\n",result->num_maxUser,  result->ct_w_maxUser);
        }


        /* clean up */
        freeFilenames(all_webInfo, i);
        free(all_webInfo);

        closedir(dir);
    }
    return 0;
}

/**
 * Opens each of the files it is tasked with handling and 
 * calculates the necessary values for a given query.
 * @param v: struct to be filed
 * @return the struct v
 */
static void* map(void* v){
    WebInfo *webInfo = (WebInfo*)v;
    FILE *f = fopen(webInfo->filename, "r");

    /* data format: UNIX_TIMESTAMP, IP, DURATION, COUNTRY */
    long timeStamp;
    char ip[IP_LEN];
    double duration;
    char countryCode[COUNTRY_CODE_LEN];

    int i = 0; /* counter */
    double totalDuration = 0.0;

    /* init arrays */
    initUserYearArray(webInfo);
    initCountryCodeArray(webInfo);

    /* read and store website info */
    while(fscanf(f, "%ld,%[^,],%lf,%[^\n]\n", &timeStamp, ip, &duration, countryCode) != EOF) {
        totalDuration+=duration;

        // convert time stamp to year to calculate ave usr/year
        handleTimestamp(webInfo, timeStamp);

        // tally country code 
        handleContryCode(webInfo, countryCode);
        i++;
    }
    fclose(f);

    /* calculate and store average duration */
    webInfo->aveDuration = totalDuration/i;
    /* calculate and store average user per year */
    calculateAveUser(webInfo);
    /* find country with the most users */
    findCountryWMaxUser(webInfo);

    return ((void*)webInfo);
}

/**
 * Reduces the list of web info struct to a result struct
 * store results in the struct passed in (maybe reallocate mem)
 */
static void* reduce(void* v){
    WebInfo *allWebInfo = (WebInfo*)v;
    //FIXME: allWebInfo->num_webInfo_entry is not right
// printf("allWebInfo: %p, allWebInfo->num_webInfo_entry = %d\n",allWebInfo, allWebInfo->num_webInfo_entry);
    /*
        A/B: Max/Min Average duration;
        map(): Average duration for each website
        reduce(): Max/Min average duration of all of the websites

        C/D: Max/Min Average # of users per year
        map(): Find the average user count per year 
        reduce(): Max/Min average between all sites

        E: Country with most users
        map(): Count country codes to find maximum occurence for a website 
        reduce(): Find country with the most users
    */

    //TODO: the filename in the struct should be the file whose result is the answer to the query
    if(strcmp(QUERY_STRINGS[current_query], "A")==0) {
        /* find max average duration of all of the websites */
        allWebInfo = findMaxAveDuration(allWebInfo);
        //TODO: remove data/ in filename
    } else if(strcmp(QUERY_STRINGS[current_query], "B")==0) {
        /* find min average duration of all of the websites */
        allWebInfo = findMinAveDuration(allWebInfo);

    } else if(strcmp(QUERY_STRINGS[current_query], "C")==0) {
        /* Max Average # of users per year */
        allWebInfo = findMaxAveUser(allWebInfo);

    } else if(strcmp(QUERY_STRINGS[current_query], "D")==0) {
        /* Min Average # of users per year */
        allWebInfo = findMinAveUser(allWebInfo);

    } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
        /* country with most users */
        allWebInfo = findCountryWMaxUserR(allWebInfo);
    } else {
        /* NO SUCH QUERY */
    }

    return allWebInfo;
}
