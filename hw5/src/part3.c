#include "lott.h"
#include "functions.h"
#include "header.h"

/* semaphores */
sem_t writer_lock;  // for writer 
sem_t mutex;        // for reader
int num_reader;

/* used exclusively for E */
char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN];
int uc[NUM_COUNTRY_CODE];

/* flag to indicate map threads are joined */
short isMapJoined; 

static void* map(void*);
static void* reduce(void*);

void Init_sem() {
    sem_init(&writer_lock, 0, 1);
    sem_init(&mutex, 0, 1);
    num_reader = 0;
}

/**
 * READER
 */
void Reader(FILE *fp, WebInfo *result_aggregate) {
    sem_wait(&mutex);
    num_reader++;
    if(num_reader == 1) {
        /* at least one reader is reading, block writer */
        sem_wait(&writer_lock);
    }
    sem_post(&mutex);
    
    char result[256], filename[256];
    /* read from file, update temp result */
    if(fscanf(fp, "%[^,],%s", result, filename) == 2){
        
        WebInfo *tempResult = malloc(sizeof(WebInfo));
        tempResult->filename = filename;

        /* identify and update result */
        identifyAndUpdateResult(result, tempResult, result_aggregate, cc, uc);

        free(tempResult);
        // sleep for some time so that the writer can get the lock
        //usleep(1000); 
    }
    sem_wait(&mutex);
    num_reader--;
    if(num_reader == 0) {
        /* NO reader, release writer's lock */
        sem_post(&writer_lock);
    }
    sem_post(&mutex);
}

/**
 * WRITER
 */
void Writer(const char *filename, char *buf) {
    
    sem_wait(&writer_lock);
    
    /* open file for appending */
    FILE *fp = fopen(OUTFILE, "a+");
    /* write to file */
    fputs(buf, fp);
    /* close file */
    fclose(fp);

    sem_post(&writer_lock);
    
}

int part3(size_t nthreads){

    int i, j, entryCounter;    /* counters */
    int numEntryPerThread;
    DIR *dir;               /* directory where test files are */
    struct dirent *entry;   /* directory entries */
    
    char dataDir[strlen(DATA_DIR)*sizeof(char) + 2];
    
    WebInfo *all_webInfo;  /* list of info structures */
    WebInfo *result_aggregate = malloc(sizeof(WebInfo));  /* for storing temp result */

    /* set the result's filename to NULL for reduce's comparison use */
    // TODO: free filename 
    result_aggregate->filename = NULL;  
    

    strcpy(dataDir, DATA_DIR);

    if ((dir = opendir(dataDir)) == NULL){
        perror("opendir()");
    } else {
        entryCounter = 0;
        /* Run a first pass to get the number of entries in the directory */
        while ((entry = readdir(dir)) != NULL){
            if(entry->d_type==DT_REG){
                entryCounter++;            
            }
        }
        
        /* list of info structures */
        all_webInfo = (WebInfo*)malloc(sizeof(WebInfo) * entryCounter);  
        initCC_UC(cc, uc);

        /**********************************************************
         * divide up the work according total number of entry and *
         * number of thread given                                 *
         * 
         * The first n - 1 thread are equal in entry, the last    *
         * thread may not, it could have equal or less or more 
         * entry      
         *
         * If nthreads is greater than number of entry,           
         * then 1 thread
        ***********************************************************/
        //TODO: when nthreads == 0, throw error  
        if(entryCounter < nthreads) {
            numEntryPerThread = 1;
        } else {
            numEntryPerThread = entryCounter/nthreads;
        } /* end partitioning */


        /* list of thread ids */
        pthread_t tids[nthreads];
        pthread_t tid_reduce;

        /* rewind for spawning threads */
        rewinddir(dir);

        /* read and store file names */
        i = 0;
        while ((entry = readdir(dir)) != NULL){
            /* only look at regular files, so that . and .. are excluded */
            if(entry->d_type==DT_REG){
                /* store file name */
                (all_webInfo+i)->filename = malloc((strlen(DATA_DIR) + 1 + strlen(entry->d_name))*sizeof(char) + 1);
                strcpy((all_webInfo+i)->filename, DATA_DIR);
                strcat((all_webInfo+i)->filename, "/");
                strcat((all_webInfo+i)->filename, entry->d_name);
                i++;

            }
        }
        /* */

        /* initialize locks */
        Init_sem();

        /**********************************************************
                                WRITER 
        ***********************************************************/
        isMapJoined = 0;
        /* assign portion for spawning threads */
        j = 0;
        int portion = 1;
        for(i = 0; i < entryCounter; i+=portion){

            if(j < nthreads){
                char name[NAME_LEN];   /* thread name */

                if(j == nthreads-1) {
                    /* the last portion could have equal or more entries */
                    portion = entryCounter - j*numEntryPerThread;
                } else {
                    portion = numEntryPerThread;
                }
                (all_webInfo+i)->portion = portion;

                /* map() writes to file: "mapred.tmp" */
                pthread_create(&tids[j], NULL, map, (void*)(all_webInfo+i));
                
                /* name thread */
                sprintf(name, "map%d", j);
                pthread_setname_np(tids[j], name);
                j++;
            }
        }


        /**********************************************************
                                READER 
        ***********************************************************/
        /* create reader thread */
        pthread_create(&tid_reduce, NULL, reduce, result_aggregate/* buffer */);
        /* name thread */
        pthread_setname_np(tid_reduce, "reduce");
        
        /* join threads */
        for(i = 0; i < j; i++) {
            pthread_join(tids[i], NULL);
            // printf("joinig map%d\n", i+1);
        }
        // all maps all joined
        isMapJoined = 1;
        // /* cancel reader thread */
        // pthread_cancel(tid_reduce);
        pthread_join(tid_reduce, NULL);

        /* remove file */
        unlink(OUTFILE);

        /**********************************************************
                            print result
        ***********************************************************/
        printf("Part: %s\nQuery: %s\n", PART_STRINGS[current_part], QUERY_STRINGS[current_query]);
        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            printf("Result: %.5g, ", result_aggregate->aveDuration);
            printf("%s\n", result_aggregate->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0) {
            printf("Result: %.5g, ", result_aggregate->aveUser);
            printf("%s\n", result_aggregate->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            getMaxUserAndItsCountry(cc, uc, result_aggregate->ct_w_maxUser, &(result_aggregate->num_maxUser));
            printf("Result: %d, %s\n",result_aggregate->num_maxUser,  result_aggregate->ct_w_maxUser);
        }

        /* clean up */
        freeFilenames(all_webInfo, i);
        free(all_webInfo);
        free(result_aggregate->filename);
        free(result_aggregate);

        closedir(dir);
    }
    return 0;
}

static void* map(void* v){
    /* prevent this thread from cancelling */
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

    WebInfo *webInfo = (WebInfo*)v;   /* starting pointer to info struct */
    WebInfo *temp;      /* serves as a cursor to the current info struct */
    int i, j;              /* counter */
    int n = webInfo->portion; /* number of file to process in a thread */
    
    /* do work/map */
    for(i = 0; i < n; i++){
        /* update webInfo struct pointer */
        temp = webInfo + i;

        FILE *f = fopen(temp->filename, "r");
    
        /* data format: UNIX_TIMESTAMP, IP, DURATION, COUNTRY */
        long timeStamp;
        char ip[IP_LEN];
        double duration;
        char countryCode[COUNTRY_CODE_LEN];
        
        // int  = 0; /* counter */
        double totalDuration = 0.0;
    
        /* init arrays */
        initUserYearArray(temp);
        initCountryCodeArray(temp);
    
        j = 0;
        /* read and store website info */
        while(fscanf(f, "%ld,%[^,],%lf,%[^\n]\n", &timeStamp, ip, &duration, countryCode) != EOF) {
            if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
                totalDuration+=duration;
            } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0){
                // convert time stamp to year to calculate ave usr/year
                handleTimestamp(temp, timeStamp);
            } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
                // tally country code 
                handleContryCode(temp, countryCode);
            }
            j++;
        }
        fclose(f);


        /***********************************************
                        write
        ************************************************/
        
        char buf[256];   /* string buffer for data to be written out */

        /* calculate and store average duration */
        temp->aveDuration = totalDuration/j;
        /* calculate and store average user per year */
        calculateAveUser(temp);
        /* find country with the most users */
        findCountryWMaxUser(temp);

        /* format data to write to file */
        composeDataToWrite(buf, temp);

        /* write to file */
        Writer(OUTFILE, buf);
        
    }

    return v;
}

void cleanup_handler_pt3() {
    //TODO: finish reading the file 
    // Reader(fp, result_aggregate);
    // if query E, call getMaxUserAndItsCountry();
    /* delete temp file */
    // unlink(OUTFILE);

    // printf("unlink\n");
}


/**
 * Reduces the list of web info struct to a result struct
 * store results in the struct passed in (maybe reallocate mem)
 */
static void* reduce(void* v){
    WebInfo *result_aggregate = (WebInfo*)v;
    FILE *fp;

    fp = fopen(OUTFILE, "ab+");

    /* when thread is cancelled, print reduced result */
    // pthread_cleanup_push(cleanup_handler_pt3, NULL);

    while(1) {
        /* read data */
        Reader(fp, result_aggregate);

        /* usleep a bit so that the writer wont be starved */
        usleep(USLEEP_TIME);
        
        if(isMapJoined && feof(fp)){
            break;
        }
    }
    fclose(fp);

    // pthread_cleanup_pop(1);

    return result_aggregate;
}
