#include "functions.h"
#include "header.h"
#include "lott.h"


/**
 * Finds maximum duration among all sites
 * Returns the max duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMaxAveDuration(WebInfo *allWebInfo) {
	int i;
	double max = allWebInfo->aveDuration;
	char *filename = allWebInfo->filename;
	// char *temp;
	/* find max */
	if(allWebInfo->num_webInfo_entry > 0){
		for(i = 1; i < allWebInfo->num_webInfo_entry; i++) {
			if((allWebInfo+i)->aveDuration > max) {
				max = (allWebInfo+i)->aveDuration;
				
				filename = (allWebInfo+i)->filename;
			} else if((allWebInfo+i)->aveDuration == max) { 
				/* take the file by lexicographical order */
				if(strcmp((allWebInfo+i)->filename, filename) < 0) {
					max = (allWebInfo+i)->aveDuration;
					filename = (allWebInfo+i)->filename;
				}
			}
		}
		allWebInfo->aveDuration = max;
		// realloc 
		allWebInfo->filename = realloc(allWebInfo->filename, strlen(filename)*sizeof(char)+1);
		strcpy(allWebInfo->filename, filename);
		
		return allWebInfo;
	} else {
		return NULL;
	}
	
}

/**
 * Finds minimum duration among all sites
 * Returns the min duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMinAveDuration(WebInfo *allWebInfo) {
	
	int i;
	double min = allWebInfo->aveDuration;
	char *filename = allWebInfo->filename;

	/* find min */
	if(allWebInfo->num_webInfo_entry > 0){
		for(i = 1; i < allWebInfo->num_webInfo_entry; i++) {
			if((allWebInfo+i)->aveDuration < min) {
				min = (allWebInfo+i)->aveDuration;
				filename = (allWebInfo+i)->filename;
			} else if((allWebInfo+i)->aveDuration == min) { 
				/* take the file by lexicographical order */
				if(strcmp((allWebInfo+i)->filename, filename) < 0) {
					min = (allWebInfo+i)->aveDuration;
					filename = (allWebInfo+i)->filename;
				}
			}
		}

		allWebInfo->aveDuration = min;
		allWebInfo->filename = realloc(allWebInfo->filename, strlen(filename)*sizeof(char)+1);
		strcpy(allWebInfo->filename, filename);
		
		return allWebInfo;

	} else {
		return NULL;
	}
}


void processQuery(WebInfo *reducedWebInfo) {
	
	double result = 0.0;
	char *resultstring = NULL;

	printf("Part: %s\nQuery: %s\nResult: %.5g, %s\n", 
        PART_STRINGS[current_part], QUERY_STRINGS[current_query], result, resultstring);
}


/**
 * Frees memory allocated to filenames 
 */
void freeFilenames(WebInfo *all_webInfo, int num_filename) {
	int i;
	for(i=0; i<num_filename; i++) {
		free(all_webInfo[i].filename);
	}
}


void initUserYearArray(WebInfo *webInfo) {
	int i;
	for(i = 0; i < NUM_YEAR; i++) {
		webInfo->user_yr_counter[i] = 0;
	}
}

/**
 * Stores time stamp in epoch to web info struct as year 
 */
void handleTimestamp(WebInfo *webInfo, long timeStamp) {
	time_t time;
	struct tm *tmstruct = malloc(sizeof(*tmstruct));
	char buf_yr[5]; /* buffer for year read from time stamp */

	int indexOfYr;

	time = timeStamp;
	localtime_r(&time, tmstruct);
    strftime(buf_yr, sizeof(buf_yr), "%Y", tmstruct);

    indexOfYr = atoi(buf_yr) - START_YEAR;
    /* add user count to corresponding year */
    webInfo->user_yr_counter[indexOfYr]+=1;

    free(tmstruct);
}

void calculateAveUser(WebInfo *webInfo) {
	
	int yr_counter = 0;
	double totalUser = 0.0;
	int i;
	for(i = 0; i < NUM_YEAR; i++) {\
		if(webInfo->user_yr_counter[i]>0) {
			yr_counter++;
			totalUser+=(webInfo->user_yr_counter[i]);\
		}
	}
	webInfo->aveUser = totalUser/yr_counter;

}

/**
 * Finds maximum average user per year among all sites
 * Returns the max duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMaxAveUser(WebInfo *allWebInfo) {
	double max = allWebInfo->aveUser;
	int i;
	char *filename = allWebInfo->filename;

	/* find max */
	if(allWebInfo->num_webInfo_entry > 0){
		for(i = 1; i < allWebInfo->num_webInfo_entry; i++) {
			if((allWebInfo+i)->aveUser > max) {
				max = (allWebInfo+i)->aveUser;
				filename = (allWebInfo+i)->filename;
			} else if((allWebInfo+i)->aveUser == max) { 
				/* take the file by lexicographical order */
				if(strcmp((allWebInfo+i)->filename, filename) < 0) {
					max = (allWebInfo+i)->aveUser;
					filename = (allWebInfo+i)->filename;
				}
			}
		}

		allWebInfo->aveUser = max;
		// realloc 
		allWebInfo->filename = realloc(allWebInfo->filename, strlen(filename)*sizeof(char)+1);
		strcpy(allWebInfo->filename, filename);
		
		return allWebInfo;
	} else {
		return NULL;
	}
	
}

/**
 * Finds minimum average user per year among all sites
 * Returns the min duration and its corresponding filename 
 * by reusing and reallocating mem to the struct passed in
 */
WebInfo* findMinAveUser(WebInfo *allWebInfo) {
	double min = allWebInfo->aveUser;
	int i;
	char *filename = allWebInfo->filename;

	/* find min */
	if(allWebInfo->num_webInfo_entry > 0){
		for(i = 1; i < allWebInfo->num_webInfo_entry; i++) {
			if((allWebInfo+i)->aveUser < min) {
				min = (allWebInfo+i)->aveUser;
				filename = (allWebInfo+i)->filename;
			} else if((allWebInfo+i)->aveUser == min) { 
				/* take the file by lexicographical order */
				if(strcmp((allWebInfo+i)->filename, filename) < 0) {
					min = (allWebInfo+i)->aveUser;
					filename = (allWebInfo+i)->filename;
				}
			}
		}

		allWebInfo->aveUser = min;
		// realloc 
		allWebInfo->filename = realloc(allWebInfo->filename, strlen(filename)*sizeof(char)+1);
		strcpy(allWebInfo->filename, filename);
		
		return allWebInfo;
	} else {
		return NULL;
	}
	
}

void initCountryCodeArray(WebInfo *webInfo) {
	int i;
	for(i = 0; i < NUM_COUNTRY_CODE; i++) {
		strcpy(webInfo->countryCode[i], "-");
		webInfo->countryCodeCounter[i] = 0;	/* set counter to 0 */
	}
}

/**
 * updates counter for each country code read 
 */
void handleContryCode(WebInfo *webInfo, char countryCode[]) {
	int i;
	for(i = 0; i < NUM_COUNTRY_CODE; i++) {
		if(strcmp(webInfo->countryCode[i], countryCode)==0) {
			/* match, increment count */
			webInfo->countryCodeCounter[i]++;
			break;
		} else if(strcmp(webInfo->countryCode[i], "-")==0) {
			/* country code havent been added to array yet, 
			   add it and increment count */
			strcpy(webInfo->countryCode[i], countryCode);
			webInfo->countryCodeCounter[i]++;
			break;
		}
	}
}

void findCountryWMaxUser(WebInfo *webInfo) {
	int i;
	int max = webInfo->countryCodeCounter[0];
	int indexOfMax = 0;
	for(i = 1; i < NUM_COUNTRY_CODE; i++) {
		if(webInfo->countryCodeCounter[i] > max) {
			max = webInfo->countryCodeCounter[i];
			indexOfMax = i;
		}
	}
	strcpy(webInfo->ct_w_maxUser, webInfo->countryCode[indexOfMax]);
	webInfo->num_maxUser = max;

}

/**
 * Finds the country with most users among ALL sites
 * R at the end stands for reduce
 */
WebInfo* findCountryWMaxUserR(WebInfo *allWebInfo) {
	
	int i, j;	/* counter  */
	
	/* sum up the countries in each site, get the max among them */
	/* procedure :
			1. create an char array of country codes, call it cc, init to "-"
			2. create a corresponding array of user counts, call it uc, init to 0
			2. for every country with max number of user in each file
				go through cc, 
				1. if match, add user count to uc[i]
				2. if "-", strcpy(cc[i], country code), add user count to uc[i]
	*/
	char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN];
	int uc[NUM_COUNTRY_CODE];

	initCC_UC(cc, uc);
	
	/* find the site that has the country with most users  */
	if(allWebInfo->num_webInfo_entry > 0){
		/* go through the entries to add up the same country w/max user */
		for(i = 0; i < allWebInfo->num_webInfo_entry; i++) {

			for(j = 0; j < NUM_COUNTRY_CODE; j++) {
				if(strcmp((allWebInfo+i)->ct_w_maxUser, cc[j])==0) {
					/* match, increment count */
					uc[j]+=(allWebInfo+i)->num_maxUser;
					break;
				} else if(strcmp(cc[j], "-")==0) {
					/* country code havent been added to array yet, 
					   add it and increment count */
					strcpy(cc[j], (allWebInfo+i)->ct_w_maxUser);
					uc[j]+=(allWebInfo+i)->num_maxUser;
					break;
				}
			}
		}

		getMaxUserAndItsCountry(cc, uc, allWebInfo->ct_w_maxUser, &(allWebInfo->num_maxUser));

		return allWebInfo;
	} else {
		return NULL;
	}
	
}

/**
 * Compares "tempResult" data with "result's" 
 * and updates if applicable 
 */
void updateTempResult(WebInfo *tempResult, WebInfo *result_aggregate, 
					  char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc) {

    if(strcmp(QUERY_STRINGS[current_query], "A")==0) {
        /* find max average duration of all of the websites */
        if(result_aggregate->filename == NULL) {
            result_aggregate->aveDuration = tempResult->aveDuration;
            result_aggregate->filename =  malloc(strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);

        } else if(result_aggregate->aveDuration < tempResult->aveDuration) {
            result_aggregate->aveDuration = tempResult->aveDuration;
            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);

        } else if(result_aggregate->aveDuration == tempResult->aveDuration) {
        	if(strcmp(result_aggregate->filename, tempResult->filename) > 0){
	            result_aggregate->aveDuration = tempResult->aveDuration;
	            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
	            strcpy(result_aggregate->filename, tempResult->filename);
	        }
        } 
    } else if(strcmp(QUERY_STRINGS[current_query], "B")==0) {
        /* find min average duration of all of the websites */
        if(result_aggregate->filename == NULL) {
            result_aggregate->aveDuration = tempResult->aveDuration;
            result_aggregate->filename =  malloc(strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);

        } else if(result_aggregate->aveDuration > tempResult->aveDuration) {
            result_aggregate->aveDuration = tempResult->aveDuration;
            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);

        } else if(result_aggregate->aveDuration == tempResult->aveDuration) {
        	if(strcmp(result_aggregate->filename, tempResult->filename) > 0){
	            result_aggregate->aveDuration = tempResult->aveDuration;
	            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
	            strcpy(result_aggregate->filename, tempResult->filename);
	        }
        } 

    } else if(strcmp(QUERY_STRINGS[current_query], "C")==0) {
        /* Max Average # of users per year */
        if(result_aggregate->filename == NULL) {
            result_aggregate->aveUser = tempResult->aveUser;
            result_aggregate->filename = malloc(strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);
        } else if(result_aggregate->aveUser < tempResult->aveUser) {
            result_aggregate->aveUser = tempResult->aveUser;
            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);

        } else if(result_aggregate->aveUser == tempResult->aveUser) {
        	if(strcmp(result_aggregate->filename, tempResult->filename) > 0){
	            result_aggregate->aveUser = tempResult->aveUser;
	            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
	            strcpy(result_aggregate->filename, tempResult->filename);
	        }
        } 

    } else if(strcmp(QUERY_STRINGS[current_query], "D")==0) {
        /* Min Average # of users per year */
        if(result_aggregate->filename == NULL) {
            result_aggregate->aveUser = tempResult->aveUser;
            result_aggregate->filename = malloc(strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);
        } else if(result_aggregate->aveUser > tempResult->aveUser) {
            result_aggregate->aveUser = tempResult->aveUser;
            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
            strcpy(result_aggregate->filename, tempResult->filename);

        } else if(result_aggregate->aveUser == tempResult->aveUser) {
        	if(strcmp(result_aggregate->filename, tempResult->filename) > 0){
	            result_aggregate->aveUser = tempResult->aveUser;
	            result_aggregate->filename = realloc(result_aggregate->filename, strlen(tempResult->filename)+1);
	            strcpy(result_aggregate->filename, tempResult->filename);
	        }
	    }

    } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
    	//FIXME: if it is E, collect all the counts of user for each file

    	int j;
    	
    	/* add up the max user count for each country */
    	for(j = 0; j < NUM_COUNTRY_CODE; j++) {
			if(strcmp(tempResult->ct_w_maxUser, cc[j])==0) {
				/* match, increment count */
				uc[j]+=tempResult->num_maxUser;
				break;
			} else if(strcmp(cc[j], "-")==0) {
				/* country code havent been added to array yet, 
				   add it and increment count */
				strcpy(cc[j], tempResult->ct_w_maxUser);
				uc[j]+=tempResult->num_maxUser;
				break;
			}
		}

    }
}

void composeDataToWrite(char *buf, WebInfo *temp) {

        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            /* fill buf with info and filenames */
            sprintf(buf, "%lf", temp->aveDuration);
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0){
            /* fill buf with info and filenames */
            sprintf(buf, "%lf", temp->aveUser); 
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            /* fill buf with info and filenames */
            sprintf(buf, "%d", temp->num_maxUser);
            //TODO: need to add which country has the max user
            strcat(buf, "|");
            strcat(buf, temp->ct_w_maxUser);
        }

        /* fill up the buffer */
        strcat(buf, ",");
        strcat(buf, temp->filename);
        strcat(buf, "\n");
}

void identifyAndUpdateResult(char *result, WebInfo *tempResult, WebInfo *result_aggregate, 
							 char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc) {
	/* identify what the desired result is */
    if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
        sscanf(result, "%lf", &(tempResult->aveDuration));
    } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0){
        sscanf(result, "%lf", &(tempResult->aveUser));
    } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
        //break result(which of the form [0-9*]|[a-z*]) into number of max use and its country  
        sscanf(result, "%d|%s", &(tempResult->num_maxUser), tempResult->ct_w_maxUser);
    }
    updateTempResult(tempResult, result_aggregate, cc, uc);
}

void initCC_UC(char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc) {
	int i;
	for(i = 0; i < NUM_COUNTRY_CODE; i++) {
		strcpy(cc[i], "-");
		uc[i] = 0;
	}
}

void getMaxUserAndItsCountry(char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN], int *uc, char *country, int *num_maxUser) {
	int i;
	int max;	/* temp max */
	char ct_w_maxUser[COUNTRY_CODE_LEN];
	/* find max user and its country */
	max = uc[0];
	strcpy(ct_w_maxUser, cc[0]);
	for(i = 1; i < NUM_COUNTRY_CODE; i++) {
		if(uc[i] > max) {
			max = uc[i];
			strcpy(ct_w_maxUser, cc[i]);
		} else if(uc[i] == max) {
			/* take the country with the preceding order lexicographically */
			if(strcmp(ct_w_maxUser, cc[i]) < 0) {
				strcpy(ct_w_maxUser, cc[i]);
			}
		}

	}

	/* store result */
	strcpy(country, ct_w_maxUser);
	*num_maxUser = max;
}
