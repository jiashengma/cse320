#include "lott.h"
#include "functions.h"
#include "header.h"

static void* map(void*);
static void* reduce(void*);

/* buffer for consumer and producer */
global_buf g_buffer;

/* used exclusively for E */
char cc[NUM_COUNTRY_CODE][COUNTRY_CODE_LEN];
int uc[NUM_COUNTRY_CODE];


/* flag to indicate map threads are joined */
// short isMapJoined; 

void init_buf(global_buf *g_buffer, int buffer_size) {
    g_buffer->webInfo_result_buf = malloc(sizeof(WebInfo) * buffer_size);
    g_buffer->n = buffer_size;
    g_buffer->index_head = g_buffer->index_tail = 0;
    sem_init(&g_buffer->mutex, 0, 1);
    sem_init(&g_buffer->empty_slot, 0, 1);
    sem_init(&g_buffer->filled_slot, 0, 0);
}

void free_buf(global_buf *bf) {
    free(bf->webInfo_result_buf);
}

/* producer */
void insert_to_buf(global_buf *g_buffer, WebInfo *tempResult) {
    sem_wait(&g_buffer->empty_slot);     /* P: decrement number of empty slot by 1 */
    sem_wait(&g_buffer->mutex);  

    // insert: map the result to buffer 
    // g_buffer->webInfo_result_buf = tempResult;
    (g_buffer->webInfo_result_buf[(++g_buffer->index_tail)%(g_buffer->n)]) = *tempResult;

    sem_post(&g_buffer->mutex);
    sem_post(&g_buffer->filled_slot);    /* V: increment number of filled slot by 1 */
}

/* consumer */
WebInfo *consume_buf(global_buf *g_buffer, WebInfo *result_aggregate) {
    WebInfo *item_to_remove;
    sem_wait(&g_buffer->filled_slot);    /* P: decrement number of filled slot by 1 */
    sem_wait(&g_buffer->mutex);  

    // remove: read the result from buffer and empty it by increasing empty slot 
    item_to_remove = &(g_buffer->webInfo_result_buf[(++g_buffer->index_head)%(g_buffer->n)]);
    updateTempResult(item_to_remove, result_aggregate, cc, uc);
    
    sem_post(&g_buffer->mutex);
    sem_post(&g_buffer->empty_slot);     /* V: increment number of empty slot by 1 */
    return item_to_remove;
}

int part4(size_t nthreads){

    int i, j, entryCounter; /* counters */
    int numEntryPerThread;

    DIR *dir;               /* directory where test files are */
    struct dirent *entry;   /* directory entries */
    
    char dataDir[strlen(DATA_DIR)*sizeof(char) + 2];
    
    WebInfo *all_webInfo;  /* list of all info structures */

    WebInfo *result_aggregate = malloc(sizeof(WebInfo));      /* for storing temp result */
    
    /* set the result_aggregate's filename to NULL for reduce's comparison use */
    result_aggregate->filename = NULL;   

    strcpy(dataDir, DATA_DIR);

    /* initialize global buffer */
    init_buf(&g_buffer, BUFFER_SIZE);

    if ((dir = opendir(dataDir)) == NULL){
        perror("opendir()");
    } else {
        entryCounter = 0;
        /* Run a first pass to get the number of entries in the directory */
        while ((entry = readdir(dir)) != NULL){
            if(entry->d_type==DT_REG){
                entryCounter++;            
            }
        }
        
        /* list of info structures */
        all_webInfo = (WebInfo*)malloc(sizeof(WebInfo) * entryCounter);  
        initCC_UC(cc, uc);

        /**********************************************************
         * divide up the work according total number of entry and *
         * number of thread given                                 *
         * 
         * The first n - 1 thread are equal in entry, the last    *
         * thread may not, it could have equal or less or more 
         * entry      
         *
         * If nthreads is greater than number of entry,           
         * then 1 thread
        ***********************************************************/
        //TODO: when nthreads == 0, throw an error
        if(entryCounter < nthreads) {
            numEntryPerThread = 1;
        } else {
            numEntryPerThread = entryCounter/nthreads;
        }
        /* list of thread ids */
        pthread_t tids[nthreads];
        pthread_t tid_reduce;

        /* rewind for spawning threads */
        rewinddir(dir);

        /* read and store file names */
        i = 0;
        while ((entry = readdir(dir)) != NULL){
            /* only look at regular files, so that . and .. are excluded */
            if(entry->d_type==DT_REG){
                /* store file name */
                (all_webInfo+i)->filename = malloc((strlen(DATA_DIR) + 1 + strlen(entry->d_name))*sizeof(char) + 1);
                strcpy((all_webInfo+i)->filename, DATA_DIR);
                strcat((all_webInfo+i)->filename, "/");
                strcat((all_webInfo+i)->filename, entry->d_name);
                i++;
            }
        }

        /**********************************************************
                                WRITER
        ***********************************************************/
        // isMapJoined = 0;
        /* assign portion for spawning threads */
        j = 0;
        int portion = 1;
        for(i = 0; i < entryCounter; i+=portion){

            if(j < nthreads){
                char name[NAME_LEN];   /* thread name */

                // int portion;
                if(j == nthreads-1) {
                    /* the last portion could have equal or more entries */
                    portion = entryCounter - j*numEntryPerThread;
                } else {
                    portion = numEntryPerThread;
                }
                (all_webInfo+i)->portion = portion;

                /* map() writes to file: "mapred.tmp" */
                pthread_create(&tids[j], NULL, map, (void*)(all_webInfo+i));
                
                /* name thread */
                sprintf(name, "map%d", j);
                pthread_setname_np(tids[j], name);
                j++;
            }
        }

        /**********************************************************
                                READER
        ***********************************************************/
        
        /* create reader thread */
        pthread_create(&tid_reduce, NULL, reduce, result_aggregate/* buffer */);
        /* name thread */
        pthread_setname_np(tid_reduce, "reduce");
        

        /*********************************************
                        conclude
        *********************************************/
        /* join threads */
        for(i = 0; i < j; i++) {
            pthread_join(tids[i], NULL);
        }
        // isMapJoined = 1;

        /* suspend the main thread after map is joined 
           so that reduce finishes up the last data set */
        usleep(USLEEP_TIME);

        /* cancel reader thread */
        pthread_cancel(tid_reduce);
        // pthread_join(tid_reduce, NULL);

        /* print result */
        printf("Part: %s\nQuery: %s\n", PART_STRINGS[current_part], QUERY_STRINGS[current_query]);
        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            printf("Result: %.5g, ", result_aggregate->aveDuration);
            printf("%s\n", result_aggregate->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0) {
            printf("Result: %.5g, ", result_aggregate->aveUser);
            printf("%s\n", result_aggregate->filename+strlen(DATA_DIR)+1);
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            getMaxUserAndItsCountry(cc, uc, result_aggregate->ct_w_maxUser, &(result_aggregate->num_maxUser));
            printf("Result: %d, %s\n",result_aggregate->num_maxUser,  result_aggregate->ct_w_maxUser);
        }

        /* clean up */
        freeFilenames(all_webInfo, i);
        free(all_webInfo);
        free(result_aggregate->filename);
        free(result_aggregate);
        free_buf(&g_buffer);

        closedir(dir);
    }
    return 0;
}

static void* map(void* v){
    /* prevent this thread from cancelling */
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

    WebInfo *webInfo = (WebInfo*)v;   /* starting pointer to info struct */
    WebInfo *temp;      /* serves as a cursor to the current info struct */

    /* counter */
    int i, j;
   
    /* number of file to process in a thread */
    int n = webInfo->portion;
    
    /* do work/map */
    for(i = 0; i < n; i++){
        /* update webInfo struct pointer */
        temp = webInfo + i;

        FILE *f = fopen(temp->filename, "r");
    
        /* data format: UNIX_TIMESTAMP, IP, DURATION, COUNTRY */
        long timeStamp;
        char ip[IP_LEN];
        double duration;
        char countryCode[COUNTRY_CODE_LEN];
    
        //int i = 0; /* counter */
        double totalDuration = 0.0;
    
        /* init arrays */
        initUserYearArray(temp);
        initCountryCodeArray(temp);
    
        j = 0;
        /* read and store website info */
        while(fscanf(f, "%ld,%[^,],%lf,%[^\n]\n", &timeStamp, ip, &duration, countryCode) != EOF) {
            if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
                totalDuration+=duration;
            } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0){
                // convert time stamp to year to calculate ave usr/year
                handleTimestamp(temp, timeStamp);
            } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
                // tally country code 
                handleContryCode(temp, countryCode);
            }
            j++;
        }
        if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
            /* calculate and store average duration */
            temp->aveDuration = totalDuration/j;
            
        } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0){
            /* calculate and store average user per year */
            calculateAveUser(temp);
        
        } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
            /* find country with the most users */
            findCountryWMaxUser(temp);

        }
        fclose(f);
        /* END read and store website info */

        /***********************************************
                        produce
        ************************************************/
        insert_to_buf(&g_buffer, temp);

    }

    return v;
}

void cleanup_handler(void *arg) {
    WebInfo *result = (WebInfo*)arg;

    /* print result */
    printf("Part: %s\nQuery: %s\n", PART_STRINGS[current_part], QUERY_STRINGS[current_query]);
    if(strcmp(QUERY_STRINGS[current_query], "A")==0 || strcmp(QUERY_STRINGS[current_query], "B")==0){
        printf("Result: %.5g, ", result->aveDuration);
        printf("%s\n", result->filename+strlen(DATA_DIR)+1);
    } else if(strcmp(QUERY_STRINGS[current_query], "C")==0 || strcmp(QUERY_STRINGS[current_query], "D")==0) {
        printf("Result: %.5g, ", result->aveUser);
        printf("%s\n", result->filename+strlen(DATA_DIR)+1);
    } else if(strcmp(QUERY_STRINGS[current_query], "E")==0) {
        //TODO: check the right output 
        printf("Result: %d, %s\n",result->num_maxUser,  result->ct_w_maxUser);
    }
}

/**
 * Reduces the list of web info struct to a result struct
 * store results in the struct passed in (maybe reallocate mem)
 */
static void* reduce(void* v){
   
    WebInfo *result = (WebInfo*)v;

    /* when thread is cancelled, print reduced result */
    // pthread_cleanup_push(cleanup_handler, result);

    /* read info from buffer and update temp result */
    while(1){ 
        consume_buf(&g_buffer, result);

        // if(isMapJoined) {
        //     break;
        // }
    }

    // pthread_cleanup_pop(0);

    return result;
}
